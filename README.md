# AoC 2022

This year I am trying to learn Rust by completing the AoC challenges in Rust. However, the first time I ever used Rust is when AoC started, and I'm also going for speed on our private leaderboard, so sometimes I'm using Python to compromise between the two goals.
I also want to compare the efficiency of different solutions, including Python solutions.
To run a given solution, change into the `src/bin` folder and the run `cargo run --bin [day_part]`
