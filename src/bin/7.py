with open('7.input') as f:
    lines = f.read()
    path = []
    cur_dir = ""
    ls = False
    sizes = {}
    dirs = {}
    finals = {}
    for line in lines.split("\n"):
        if not line:
            print("SKIPPED")
            continue
        args = line.split(" ")
        if args[0] == "$":
            ls = False;
            if args[1] == "cd":
                if args[2] == "..":
                    path.pop()
                else:
                    path.append(args[2])
                cur_dir = "/".join(path)
                dirs.setdefault(cur_dir, set())
                sizes.setdefault(cur_dir, 0)
            if args[1] == "ls":
                ls = True;
        elif ls:
            if args[0] == "dir":
                dirs.setdefault(cur_dir, set())
                dirs[cur_dir].add(cur_dir + '/' + args[1])
            else:
                sizes.setdefault(cur_dir, 0)
                sizes[cur_dir] += int(args[0])

    # print(dirs)
    # print(sizes)

    while len(finals) < len(dirs):
        for d, children in dirs.items():
            if len(children) == 0:
                finals.setdefault(d, sizes[d])
            else:
                children_ready = True
                for c in children:
                    if not c in finals:
                        children_ready = False
                if children_ready:
                    finals.setdefault(d, sum(map(lambda x: finals[x], children)) + sizes[d])

    # print(finals)
    
    # t = 0
    # for _, s in finals.items():
    #     if s >= 1000 and s <= m:
    #         t = s
    m = 70000000
    for _, s in finals.items():
        if s >= 3629016 and s <= m:
            m = s
    # m = 0
    # for _, s in finals.items():
    #     if s >= m:
    #         m = s
    print(70000000 - 43629016)
    print(30000000 - 26370984)
    print(m)
