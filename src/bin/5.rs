use std::fs;

//                         [Z] [W] [Z]
//         [D] [M]         [L] [P] [G]
//     [S] [N] [R]         [S] [F] [N]
//     [N] [J] [W]     [J] [F] [D] [F]
// [N] [H] [G] [J]     [H] [Q] [H] [P]
// [V] [J] [T] [F] [H] [Z] [R] [L] [M]
// [C] [M] [C] [D] [F] [T] [P] [S] [S]
// [S] [Z] [M] [T] [P] [C] [D] [C] [D]
//  1   2   3   4   5   6   7   8   9 
fn main() {
    // Setup stacks
    let mut stacks: [Vec<char>; 10] = [
        vec![], // element 0 not used
        vec!['S', 'C', 'V', 'N'],
        vec!['Z', 'M', 'J', 'H', 'N', 'S'],
        vec!['M', 'C', 'T', 'G', 'J', 'N', 'D'],
        vec!['T', 'D', 'F', 'J', 'W', 'R', 'M'],
        vec!['P', 'F', 'H'],
        vec!['C', 'T', 'Z', 'H', 'J'],
        vec!['D', 'P', 'R', 'Q', 'F', 'S', 'L', 'Z'],
        vec!['C', 'S', 'L', 'H', 'D', 'F', 'P', 'W'],
        vec!['D', 'S', 'M', 'P', 'F', 'N', 'G', 'Z'],
    ];

    // Get input
    let file_path = "5.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let moves = input.split_terminator("\n");
    for mover in moves {
        let steps: Vec<&str> = mover.split_terminator(' ').collect();
        for _ in 1..=steps[1].parse().unwrap() {
            // Move
            let to_stack: usize = steps[5].parse().unwrap();
            let from_stack: usize = steps[3].parse().unwrap();
            let item: char = stacks[from_stack].pop().unwrap();
            stacks[to_stack].push(item);
        }
    }

    // Print stack tops
    for s in stacks {
        if s.len() > 0 {
            print!("{}", s.last().unwrap());
        }
    }
    println!("");
}

