use std::fs;
use std::collections::HashSet;

fn main() {
    // Get input
    // const WIDTH: isize = 20;
    // let file_path = "15.example";
    const WIDTH: isize = 4000000;
    let file_path = "15.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines = input.split_terminator("\n");

    type Point = (isize, isize);
    let mut sensors: Vec<(Point, Point, isize)> = Vec::new();
    let mut beacons: HashSet<Point> = HashSet::new();

    // Build the sensors and beacons
    for line in lines {
        let terms: Vec<&str> = line.split_terminator(['=', ',', ':']).collect();
        let s_x: isize = terms[1].parse().unwrap();
        let s_y: isize = terms[3].parse().unwrap();
        let b_x: isize = terms[5].parse().unwrap();
        let b_y: isize = terms[7].parse().unwrap();
        let dist = (s_x - b_x).abs() + (s_y - b_y).abs();
        sensors.push(((s_x, s_y), (b_x, b_y), dist));
        beacons.insert((b_x, b_y));
    }

    // Find empty spaces
    type Range = (isize, isize);
    let mut ranges: Vec<Range>;
    // let y=10;
    let mut found_x = 0;
    let mut found_y = 0;
    for y in 0..WIDTH + 1 {
        // RESET RANGE!
        ranges = Vec::new();
        for sensor in &sensors {
            // Use ranges
            let y_diff = (sensor.0.1 - y).abs();
            let offset = sensor.2 - y_diff;
            let left = sensor.0.0 - (offset).abs();
            let right = sensor.0.0 + (offset).abs();
            if y_diff <= sensor.2 {
                if y_diff == sensor.2 {
                    ranges.push((sensor.0.0, sensor.0.0));
                } else {
                    ranges.push((left, right));
                }
            }
        }
        // Sort to ensure we are good.
        ranges.sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        // Ensure ranges overlap.
        let mut m = ranges[0].1;
        let mut uncovered: HashSet<Range> = HashSet::new();
        for range in ranges {
            // print!("({}, {}):{}, ", range.0, range.1, m);
            uncovered.retain(|u| {
                if range.0 <= u.0 && range.1 >= u.1 {
                    return false;
                }
                return true;
            });
            if range.0 > m + 1 && range.0 < WIDTH - 1 {
                // print!("--INSERTING({},{})--", m + 1, range.0 - 1);
                uncovered.insert((m + 1, range.0 - 1));
            }
            if range.1 >= m {
                m = range.1;
            }
        }
        if uncovered.len() > 0 {
            // THERE IS A GAP!
            println!("FOUND");
            found_y = y;
            for u in uncovered {
                found_x = u.0;
            }
            break;
        }
        // println!("y: {}", y);
    }

    println!("X: {}", found_x);
    println!("Y: {}", found_y);
    println!("Part 2: {}", found_x * 4000000 + found_y);
}
