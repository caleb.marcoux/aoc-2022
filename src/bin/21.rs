use std::fs;
use std::collections::HashMap;

fn main() {
    // Get input
    // let file_path = "21.example";
    let file_path = "21.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines = input.split_terminator("\n");
    let mut monkeys: HashMap<&str, isize> = HashMap::new();
    let mut exprs: HashMap<&str, (&str, &str, &str)> = HashMap::new();

    for line in lines {
        // Parse input
        let terms: Vec<&str> = line.split_terminator(": ").collect();
        let try_num = terms[1].parse::<isize>();
        match try_num {
            Ok(num) => {
                monkeys.insert(terms[0], num);
            },
            Err(_) => {
                let ops: Vec<&str> = terms[1].split(' ').collect();
                exprs.insert(terms[0], (ops[0], ops[1], ops[2]));
            },
        };
    }

    // Loop until all monkeys (or root) numbers are known.
    let mut not_found = true;
    while not_found {
        for (name, expr) in &exprs {
            if monkeys.contains_key(expr.0) && monkeys.contains_key(expr.2) {
                let a = monkeys.get(expr.0).unwrap();
                let b = monkeys.get(expr.2).unwrap();
                monkeys.insert(name, match expr.1 {
                    "+" => a + b,
                    "*" => a * b,
                    "-" => a - b,
                    "/" => a / b,
                    _ => 0,
                });
                if *name == "root" {
                    not_found = false;
                }
            }
        }
    }

    let root = monkeys.get("root").unwrap();
    println!("Part 1: {}", root);
    // println!("Part 2: {}", t);
}
