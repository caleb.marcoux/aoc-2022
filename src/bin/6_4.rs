use std::fs;

fn run(masks: &[u32], size: usize) {
    let mut accum = 0u32;
    for (i, mask) in masks.iter().enumerate() {
        accum ^= mask;
        if i >= size {
            accum ^= masks[i - size];
            if accum.count_ones() as usize == size {
                println!("{}", i + 1);
                return;
            }
        }
    }
    println!("Marker not found");
}

fn main() {
    // Get input
    let file_path = "6.input";
    // Turn the input into a vector of numbers
    let input = fs::read_to_string(file_path)
        .expect("Could not read input")
        .chars().filter_map(|c| {
            // Handle null string end
            if c as u32 >= 'a' as u32 {
                Some(1u32 << (c as u32 - 'a' as u32))
            } else {
                None
            }
        })
        .collect::<Vec<u32>>();
    
    run(&input, 4);
    run(&input, 14);
}
