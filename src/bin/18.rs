use std::fs;
use std::collections::HashSet;
use std::collections::HashMap;

fn main() {
    // Get input
    // let file_path = "18.example";
    let file_path = "18.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines: Vec<&str> = input.split_terminator('\n').collect();

    type Cube = (u32, u32, u32);
    let mut cubes: HashSet<Cube> = HashSet::new();
    let mut spaces: HashSet<Cube> = HashSet::new();

    for line in lines {
        let coords: Vec<u32> = line.split_terminator(',')
            .map(|c| c.parse().unwrap()).collect();
        let x = coords[0];
        let y = coords[1];
        let z = coords[2];
        cubes.insert((x, y, z));
    }

    // let cubes_inner = cubes.clone();
    let mut t = 0;
    for cube in &cubes {
        // println!("Cube: ({},{},{})", cube.0, cube.1, cube.2);
        // whether each face is open
        let mut free = 0;
        let mut x_b = true;
        let mut y_b = true;
        let mut z_b = true;
        let mut x_f = true;
        let mut y_f = true;
        let mut z_f = true;
        for other in &cubes {
        // println!("Other: ({},{},{})", other.0, other.1, other.2);
            if cube != other {
                // Check if side is covered
                if cube.0 == other.0 + 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_f = false;
                }
                if other.0 > 0 && cube.0 == other.0 - 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_b = false;
                }
                if cube.1 == other.1 + 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_f = false;
                }
                if other.1 > 0 && cube.1 == other.1 - 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_b = false;
                }
                if cube.2 == other.2 + 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_f = false;
                }
                if other.2 > 0 && cube.2 == other.2 - 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_b = false;
                }
            }
        }
        // Add up free sides
        if x_f {
            free += 1;
            spaces.insert((cube.0 + 1, cube.1, cube.2));
        }
        if x_b {
            free += 1;
            spaces.insert((cube.0 - 1, cube.1, cube.2));
        }
        if y_f {
            free += 1;
            spaces.insert((cube.0, cube.1 + 1, cube.2));
        }
        if y_b {
            free += 1;
            spaces.insert((cube.0, cube.1 - 1, cube.2));
        }
        if z_f {
            free += 1;
            spaces.insert((cube.0, cube.1, cube.2 + 1));
        }
        if z_b {
            free += 1;
            spaces.insert((cube.0, cube.1, cube.2 - 1));
        }
        t += free;
    }

    // Build open faces on spaces
    let mut sides: HashMap<Cube, Vec<u32>> = HashMap::new();
    for cube in &spaces {
        if !&cubes.contains(cube) {
            // whether each face is open
            let mut x_b = true;
            let mut y_b = true;
            let mut z_b = true;
            let mut x_f = true;
            let mut y_f = true;
            let mut z_f = true;
            for other in &cubes {
                // Check if side is covered
                if cube.0 == other.0 + 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_f = false;
                }
                if other.0 > 0 && cube.0 == other.0 - 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_b = false;
                }
                if cube.1 == other.1 + 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_f = false;
                }
                if other.1 > 0 && cube.1 == other.1 - 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_b = false;
                }
                if cube.2 == other.2 + 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_f = false;
                }
                if other.2 > 0 && cube.2 == other.2 - 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_b = false;
                }
            }

            // Add to our open sides tracker
            let mut open = Vec::new();
            if x_f {
                open.push(0);
            }
            if x_b {
                open.push(1);
            }
            if y_f {
                open.push(2);
            }
            if y_b {
                open.push(3);
            }
            if z_f {
                open.push(4);
            }
            if z_b {
                open.push(5);
            }
            sides.insert(*cube, open);

            // if !x_f && !x_b && !y_f && !y_b && !z_f && !z_b {
            //     println!("SUROUNDED");
            //     t -= 6;
            // }
        }
    }

    // Subtract inner cubes
    for cube in &spaces {
        if !&cubes.contains(cube) {
            // whether each face is open
            let mut x_b = true;
            let mut y_b = true;
            let mut z_b = true;
            let mut x_f = true;
            let mut y_f = true;
            let mut z_f = true;
            for other in &spaces {
                // Check if side is covered
                if sides.get(other).unwrap().contains(&0) || (cube.0 == other.0 + 1 && cube.1 == other.1 && cube.2 == other.2) {
                    x_f = false;
                }
                if other.0 > 0 && cube.0 == other.0 - 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_b = false;
                }
                if cube.1 == other.1 + 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_f = false;
                }
                if other.1 > 0 && cube.1 == other.1 - 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_b = false;
                }
                if cube.2 == other.2 + 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_f = false;
                }
                if other.2 > 0 && cube.2 == other.2 - 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_b = false;
                }
            }
        }
    }

    // let m = 0;
    println!("Part 1: {}", t);
}
