with open('8.input') as f:
    lines = f.read()
    rows = []
    for line in lines.split("\n"):
        rows.append(list(map(lambda c: int(c), list(line))))

    print(rows.pop())

    m = 0
    for i, row in enumerate(rows):
        # print(i)
        for k, h in enumerate(row):
            # inner
            t = 0
            up = 0
            for j in reversed(range(len(rows))):
                if (j < i):
                    if rows[j][k] < h:
                        up += 1
                    else:
                        up += 1
                        break
            left = 0
            for j in reversed(range(len(row))):
                if (j < k):
                    if row[j] < h:
                        left += 1
                    else:
                        left += 1
                        break
            down = 0
            for j, n in enumerate(rows):
                if (j > i):
                    if n[k] < h:
                        down += 1
                    else:
                        down += 1
                        break
            right = 0
            for j, n in enumerate(row):
                if (j > k):
                    if n < h:
                        right += 1
                    else:
                        right += 1
                        break
            t = right * left * up * down
            if t >= m:
                m = t
    print(m)
