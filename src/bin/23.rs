use std::fs;
mod helpers;

fn print_elves(elves: &Vec<helpers::Point>) {
    let mut s_x = 10000;
    let mut s_y = 10000;
    let mut l_x = -10000;
    let mut l_y = -10000;
    for e in elves {
        if e.x < s_x {
            s_x = e.x;
        }
        if e.y < s_y {
            s_y = e.y;
        }
        if e.x > l_x {
            l_x = e.x;
        }
        if e.y > l_y {
            l_y = e.y;
        }
    }
    for y in s_y..l_y + 1 {
        for x in s_x..l_x + 1 {
            if elves.contains(&helpers::Point::new(x, y)) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

fn main() {
    // Get input
    // let file_path = "23.example";
    // let file_path = "23_2.example";
    const ROUNDS: usize = 10;
    let file_path = "23.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut elves: Vec<helpers::Point> = Vec::new();

    for (i, line) in lines.enumerate() {
        for (j, c) in line.chars().enumerate() {
            if c == '#' {
                elves.push(helpers::Point::new(j as i32, i as i32));
            }
        }
    }

    let mut moves: Vec<helpers::Point>;

    let directions: Vec<helpers::Point> = vec![
        helpers::Point::new(0, -1), // N
        helpers::Point::new(0, 1), // S
        helpers::Point::new(-1, 0), // W
        helpers::Point::new(1, 0), // E
    ];
    let adjacent: Vec<helpers::Point> = vec![
        helpers::Point::new(0, -1), // N
        helpers::Point::new(0, 1), // S
        helpers::Point::new(-1, 0), // W
        helpers::Point::new(1, 0), // E
        helpers::Point::new(1, -1), // NE
        helpers::Point::new(1, 1), // SE
        helpers::Point::new(-1, 1), // SW
        helpers::Point::new(-1, -1), // NW
    ];
    let mut check = 0;
    
    // Do rounds
    let mut n = 0;
    let mut empty = 0;
    loop {
        if n < ROUNDS || n % 10 == 0 {
            println!("n: {}", n);
        }
        if n < ROUNDS {
            print_elves(&elves);
        }
        // Refresh moves list
        moves = Vec::new();
        // Find proposed move
        let mut no_moves = true;
        'elves: for e in &elves {
            // Move only if other elf nearby
            let mut moving = false;
            for a in &adjacent {
                if elves.contains(&helpers::Point::new(a.x + e.x, a.y + e.y)) {
                    no_moves = false;
                    moving = true;
                    break;
                }
            }
            if !moving {
                moves.push(helpers::Point::new(0, 0));
                continue 'elves;
            }

            // Try each direction in order.
            for d in 0..4 {
                let dir = directions[(check + d) % 4];
                // Diagonals of dir empty
                if (dir.x != 0 && !elves.contains(&helpers::Point::new(e.x + dir.x, e.y + 0)) && !elves.contains(&helpers::Point::new(e.x + dir.x, e.y + 1)) && !elves.contains(&helpers::Point::new(e.x + dir.x, e.y + - 1))) || (dir.y != 0 && !elves.contains(&helpers::Point::new(e.x + 0, e.y + dir.y)) && !elves.contains(&helpers::Point::new(e.x + 1, e.y + dir.y)) && !elves.contains(&helpers::Point::new(e.x + - 1, e.y + dir.y))) {
                    // Add potential move
                    moves.push(dir);
                    // break;
                    continue 'elves;
                }
            }
            // Don't add any more directions for this elf
            moves.push(helpers::Point::new(0, 0));
        }

        // Stop if none of the elves need to move
        if no_moves {
            break;
        }

        // Pick next direction first next time.
        check += 1;

        // Take move if alone
        let iter_moves = moves.clone();
        'moving: for (i, m) in iter_moves.iter().enumerate() {
            // Ensure only with this move
            let e = elves[i];
            let new_pos = helpers::Point::new(m.x + e.x, m.y + e.y);
            for (j, other) in iter_moves.iter().enumerate() {
                let other_e = elves[j];
                let other_pos = helpers::Point::new(other.x + other_e.x, other.y + other_e.y);
                if i != j && other_pos == new_pos {
                    moves[i] = helpers::Point::new(0, 0);
                    continue 'moving;
                }
            }
        }

        // Do the valid moves
        for (i, m) in moves.iter().enumerate() {
            // Do the move
            let e = elves[i];
            let new_pos = helpers::Point::new(m.x + e.x, m.y + e.y);
            elves[i] = new_pos;
        }

        // Update rounds taken
        n += 1;

        // Find the containing rect
        if n == ROUNDS {
            let mut s_x = 10000;
            let mut s_y = 10000;
            let mut l_x = -10000;
            let mut l_y = -10000;
            for e in &elves {
                if e.x < s_x {
                    s_x = e.x;
                }
                if e.y < s_y {
                    s_y = e.y;
                }
                if e.x > l_x {
                    l_x = e.x;
                }
                if e.y > l_y {
                    l_y = e.y;
                }
            }
            let area = (l_x - s_x + 1) * (l_y - s_y + 1);
            empty = area - elves.len() as i32;
        }
    }


    println!("Part 1: {}", empty);
    println!("Part 2: {}", n + 1);
}
