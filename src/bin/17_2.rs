use std::fs;

fn print_fallen(fallen: &Vec<Vec<bool>>) {
    for r in fallen.iter().rev() {
        for c in r {
            if *c {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
}

fn main() {
    // Get input
    // let file_path = "17.example";
    let file_path = "17.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let moves: Vec<char> = input.chars().collect();

    let rocks: Vec<Vec<Vec<bool>>> = vec![
        vec![
            vec![true, true, true, true]
        ],
        vec![
            vec![false, true, false],
            vec![true, true, true],
            vec![false, true, false],
        ],
        vec![
            vec![false, false, true],
            vec![false, false, true],
            vec![true, true, true],
        ],
        vec![
            vec![true],
            vec![true],
            vec![true],
            vec![true],
        ],
        vec![
            vec![true, true],
            vec![true, true],
        ],
    ];

    let mut fallen = vec![
        vec![true, true, true, true, true, true, true], // floor
    ];
    const ROCKS: usize = 1000000000000;
    let mut stopped = 0;
    let mut t_height = 0;
    let mut pos: (usize, isize) = (2, 3); // Bottom left corner
    let mut i = 0;
    let mut j = 0;
    let mut stack_height; // FIND THESE
    let mut start_height = 1;
    let mut start_stopped = 0;
    let mut stack_stopped;
    let mut padded = false;
    // Find a stack that works with our input.
    'falling: while stopped < ROCKS {
        // println!("Pos: {}, {}", pos.0, pos.1);
        // Push
        let dir = moves[i];
        if dir == '>' && pos.0 + rocks[j][0].len() < 7 {
            pos.0 += 1;
        } else if dir == '<' && pos.0 > 0 {
            pos.0 -= 1;
        } 
        // if dir != '<' && dir != '>' {
        //     println!("UNEXPECTED CHAR!");
        // } else {
        //     println!("{}", dir);
        // }
        // Check collision with fallen (left to right)
        if pos.1 < 0 {
            'outer: for (r, row) in rocks[j].iter().rev().enumerate() {
                for (c, col) in row.iter().enumerate() {
                    if pos.1 + (r as isize) < 0 // In fallen array
                        && *col && fallen[(fallen.len() as isize + pos.1 + r as isize) as usize][pos.0 + c] // In occupied piece
                    {
                        // println!("MOVE BACK");
                        // Move back
                        if dir == '>' {
                            pos.0 -= 1;
                        } else if dir == '<' {
                            pos.0 += 1;
                        }
                        break 'outer;
                    }
                }
            }
        }
        // println!("Pos: {}, {}", pos.0, pos.1);
        
        // Move down
        let mut landed = false;
        // Check collision
        if pos.1 <= 0 {
            'outer: for (r, row) in rocks[j].iter().rev().enumerate() {
                for (c, col) in row.iter().enumerate() {
                    // Hit bottom
                    if pos.1 + r as isize <= 0 // In fallen array
                        && *col && fallen[((fallen.len() - 1) as isize + pos.1 + r as isize) as usize][pos.0 + c] // Below piece occupied
                    {
                        landed = true;
                        break 'outer;
                    }
                }
            }
        }
        if landed {
            // Add to fallen
            let old_len = fallen.len() as isize;
            for (r, row) in rocks[j].iter().rev().enumerate() {
                let y = (old_len + pos.1 + r as isize) as usize;
                if y >= fallen.len() {
                    fallen.push(vec![false; 7]);
                }
                // Update this row
                for (c, col) in row.iter().enumerate() {
                    if *col {
                        fallen[y][c + pos.0] = true;
                    }
                }
            }
            // Select new rock
            j = (j + 1) % 5;
            // print_fallen(&rocks[j]);
            pos = (2, 3);
            stopped += 1;
        } else {
            // Didn't land
            pos.1 -= 1;
        }
        i = (i + 1) % (moves.len() - 1);
        if j == 0 && i == 0 && !padded {
            if start_height == 1 && start_stopped == 0 {
                start_height = fallen.len();
                start_stopped = stopped;
            } else {
                padded = true;
                stack_height = fallen.len() - start_height;
                stack_stopped = stopped - start_stopped;
                println!("GOOD= stopped: {}, height: {}", stack_stopped, stack_height);
                // Pad our numbers a bunch
                let stopped_left = (ROCKS - stopped) / stack_stopped;
                t_height += stopped_left * stack_height;
                stopped += stopped_left * stack_stopped;
                println!("FINISHED PADDING!");
            }
        }
    }


    print_fallen(&fallen);
    println!("Part 2: {}", t_height + fallen.len() - 1);
}
