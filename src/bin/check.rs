// Look at this!

let op = match rest.split(' ').collect_tuple() {
      Some((m1, "+", m2)) => Op::Add(m1, m2),
      Some((m1, "-", m2)) => Op::Sub(m1, m2),
      Some((m1, "*", m2)) => Op::Mul(m1, m2),
      Some((m1, "/", m2)) => Op::Div(m1, m2),
      _ => Op::Num(rest.parse().unwrap()),
    };
