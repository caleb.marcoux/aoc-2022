use std::fs;
mod helpers;

fn main() {
    // Get input
    // let file_path = "22.example";
    // const SIZE: usize =  4;
    let file_path = "22.input";
    const SIZE: usize = 50;
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n").collect::<Vec<&str>>();

    let mut board: Vec<Vec<char>> = Vec::new();
    let mut moves: Vec<usize> = Vec::new();
    let mut dirs: Vec<i32> = Vec::new();
    // Build the board
    for (i, line) in lines.iter().enumerate() {
        if i < lines.len() - 2 {
            // In map portion
            board.push(line.chars().filter(|c| *c as i32 > 0).collect())
        } else if i == lines.len() - 1  {
            // Moves
            moves = line.split_terminator(['R', 'L']).map(|n| n.parse().unwrap()).collect();
            dirs = line.chars().filter_map(|c| {
                match c {
                    'R' => Some(1),
                    'L' => Some(-1),
                    _ => None
                }
            }).collect();
        }
    }
    
    // let mut side = 'F'; // F, B, L, R, U, D
    let mut pos = (board[0].iter().position(|c| *c == '.').unwrap(), 0);
    // Simulate movement
    let mut dir: usize = 0;
    for (i, m) in moves.iter().enumerate() {
        let row_pre = board[pos.1].iter().position(|c| *c == '.' || *c == '#').unwrap();
        let mut col_pre = 0;
        for (r, row) in board.iter().enumerate() {
            if pos.0 < row.len() && (row[pos.0] == '.' || row[pos.0] == '#') {
                col_pre = r;
                break;
            }
        }
        let mut col_post = 0;
        for (r, row) in board.iter().rev().enumerate() {
            if pos.0 < row.len() && (row[pos.0] == '.' || row[pos.0] == '#') {
                col_post = r;
                break;
            }
        }
        let row = &board[pos.1];

        // Move in direction until impeded
        for _ in 0..*m {
            // Find our current side.
            let side = match (pos.0 / SIZE, pos.1 / SIZE) {
                (1, 0) => 'U',
                (2, 0) => 'R',
                (1, 1) => 'F',
                (1, 2) => 'D',
                (0, 2) => 'L',
                (0, 3) => 'B',
                _ => 'N'
            };
            // Find the new position and direction (if off a side)
            let mut next_pos = pos;
            let mut next_dir = dir;
            if dir == 0 && pos.0 + 1 >= row.len() {
                println!("{}: ({}, {})", dir, pos.0, pos.1);
                // Off right side
                match side {
                    'R' => {
                        next_pos = (99, 149 - pos.1);
                        next_dir = 2;
                    },
                    'F' => {
                        next_pos = (100 + (pos.1 - 50), 49);
                        next_dir = 3;
                    },
                    'D' => {
                        next_pos = (149, 49 - (pos.1 - 100));
                        next_dir = 2;
                    },
                    'B' => {
                        next_pos = ((pos.1 - 150) + 50, 149);
                        next_dir = 3;
                    },
                    _ => {}
                }
                println!("{}: ({}, {})", next_dir, next_pos.0, next_pos.1);
            }
            else if dir == 2 && pos.0 < row_pre + 1 {
                println!("{}: ({}, {})", dir, pos.0, pos.1);
                // Off left side
                match side {
                    'U' => {
                        next_pos = (0, 149 - pos.1);
                        next_dir = 0;
                    },
                    'F' => {
                        next_pos = (pos.1 - 50, 100);
                        next_dir = 1;
                    },
                    'L' => {
                        next_pos = (50, 49 - (pos.1 - 100));
                        next_dir = 0;
                    },
                    'B' => {
                        next_pos = ((pos.1 - 150) + 50, 0);
                        next_dir = 1;
                    },
                    _ => {}
                }
                println!("{}: ({}, {})", next_dir, next_pos.0, next_pos.1);
            }
            else if dir == 1 && pos.1 + 1 >= board.len() - col_post {
                println!("{}: ({}, {})", dir, pos.0, pos.1);
                // Off bottom side
                match side {
                    'R' => {
                        next_pos = (99, pos.0 - 100 + 50);
                        next_dir = 2;
                    },
                    'D' => {
                        next_pos = (49, pos.0 + 100);
                        next_dir = 2;
                    },
                    'B' => {
                        next_pos = (pos.0 + 100, 0);
                        next_dir = 1;
                    },
                    _ => {}
                }
                println!("{}: ({}, {})", next_dir, next_pos.0, next_pos.1);
            }
            else if dir == 3 && pos.1 < col_pre + 1 {
                println!("{}: ({}, {})", dir, pos.0, pos.1);
                // Off top side
                match side {
                    'R' => {
                        next_pos = (pos.0 - 100, 199);
                        next_dir = 3;
                    },
                    'U' => {
                        next_pos = (0, pos.0 + 100);
                        next_dir = 0;
                    },
                    'L' => {
                        next_pos = (50, pos.0 + 50);
                        next_dir = 0;
                    },
                    _ => {}
                }
                println!("{}: ({}, {})", next_dir, next_pos.0, next_pos.1);
            } else {
                // Not off a side
                next_pos = match dir {
                    0 => (pos.0 + 1, pos.1), // E
                    2 => (pos.0 - 1, pos.1), // W
                    3 => (pos.0, pos.1 - 1), // N
                    1 => (pos.0, pos.1 + 1), // S
                    _ => (0, 0), // None
                };
            }
            // println!("pos ({}, {})", pos.0, pos.1);
            let next_char = board[next_pos.1][next_pos.0];

            if next_char == '.' {
                // Move forward
                pos = next_pos;
                dir = next_dir;
            } else {
                break;
            }
        }

        // Change direction.
        if i < moves.len() - 1 {
            dir = ((dir as i32 + dirs[i]).rem_euclid(4)) as usize;
        }
    }



    let score = 1000 * (pos.1 + 1) + 4 * (pos.0 + 1) + dir;
    if score == 4 {
        println!("BAD");
    }

    // 1204
    println!("Part 1: {}", score);
}
