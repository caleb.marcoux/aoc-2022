use std::fs;
mod helpers;
// use std::collections::HashSet;
// use std::collections::HashMap;

fn main() {
    // Get input
    // let file_path = "22.example";
    let file_path = "22.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n").collect::<Vec<&str>>();

    let mut board: Vec<Vec<char>> = Vec::new();
    let mut moves: Vec<usize> = Vec::new();
    let mut dirs: Vec<i32> = Vec::new();
    // Build the board
    for (i, line) in lines.iter().enumerate() {
        if i < lines.len() - 2 {
            // In map portion
            board.push(line.chars().filter(|c| *c as i32 > 0).collect())
        } else if i == lines.len() - 1  {
            // Moves
            moves = line.split_terminator(['R', 'L']).map(|n| n.parse().unwrap()).collect();
            dirs = line.chars().filter_map(|c| {
                match c {
                    'R' => Some(1),
                    'L' => Some(-1),
                    _ => None
                }
            }).collect();
        }
    }

    // Print board
    for row in &board {
        for col in row {
            print!("{}", col);
        }
        println!();
    }
    
    // Simulate movement
    let mut pos = (board[0].iter().position(|c| *c == '.').unwrap(), 0);
    let mut dir: usize = 0;
    for (i, m) in moves.iter().enumerate() {
        let row_pre = board[pos.1].iter().position(|c| *c == '.' || *c == '#').unwrap();
        let mut col_pre = 0;
        for (r, row) in board.iter().enumerate() {
            if pos.0 < row.len() && (row[pos.0] == '.' || row[pos.0] == '#') {
                col_pre = r;
                break;
            }
        }
        let mut col_post = 0;
        for (r, row) in board.iter().rev().enumerate() {
            if pos.0 < row.len() && (row[pos.0] == '.' || row[pos.0] == '#') {
                col_post = r;
                break;
            }
        }
        let row = &board[pos.1];

        // Move in direction until impeded
        for _ in 0..*m {
            println!("pos ({}, {})", pos.0, pos.1);
            let next_pos = match dir {
                0 => ((pos.0 + 1 - row_pre).rem_euclid(row.len() - row_pre) + row_pre, pos.1), // E
                2 => ((pos.0 as isize - 1 - row_pre as isize).rem_euclid((row.len() - row_pre).try_into().unwrap()) as usize + row_pre, pos.1), // W
                3 => (pos.0, (pos.1 as isize - 1 - col_pre as isize).rem_euclid((board.len() - col_post - col_pre).try_into().unwrap()) as usize + col_pre), // N
                1 => (pos.0, (pos.1 + 1 - col_pre).rem_euclid(board.len() - col_post - col_pre) as usize + col_pre), // S
                _ => (0, 0),
            };
            let next_char = board[next_pos.1][next_pos.0];

            if next_char == '.' {
                // Move forward
                pos = next_pos;
            } else {
                break;
            }
        }

        // Change direction.
        if i < moves.len() - 1 {
            dir = ((dir as i32 + dirs[i]).rem_euclid(4)) as usize;
        }
    }



    let score = 1000 * (pos.1 + 1) + 4 * (pos.0 + 1) + dir;
    if score == 4 {
        println!("BAD");
    }

    // 1204
    println!("Part 1: {}", score);
}
