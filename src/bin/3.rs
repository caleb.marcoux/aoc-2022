use std::fs;

fn main() {
    // Get input
    let file_path = "3.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let sacks = input.split_terminator("\n");
    let mut total = 0;
    let mut counts: Vec<i32> = vec![0; 53];
    for (g, sack) in sacks.enumerate() {
        let mut found: Vec<bool> = Vec::with_capacity(53);
        found.resize(53, false);
        // Reset group
        if g % 3 == 0 {
            counts.fill(0);
        }
        for t in sack.chars() {
            let mut v = (t as u32) + 1 - ('A' as u32);
            if v < 33 {
                v += 26;
            } else {
                v -= 32;
            }
            if !found[v as usize] {
                found[v as usize] = true;
                counts[v as usize] += 1;
                if counts[v as usize] >= 3 {
                    total += v;
                    break;
                }
            }
        }
    }
    println!("{}", total);
}

