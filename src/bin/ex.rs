use std::fs;
// mod helpers;
// use std::collections::HashSet;
// use std::collections::HashMap;

fn main() {
    // Get input
    // let file_path = "19.example";
    let file_path = "19.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let p1 = helpers::Point::new(0, 1);
    let p2 = helpers::Point::new(3, 1);
    println!("p1 + p2 = {}", p1 + p2);
    println!("p1 - p2 = {}", p1 - p2);
    helpers::print_array(&p1.neighbors());
    helpers::print_vec(&helpers::get_ints("Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 4 ore. Each obsidian robot costs 4 ore and 20 clay. Each geode robot costs 2 ore and 12 obsidian."));
    helpers::print_vec(&helpers::get_ints("Sensor at x=3907621, y=2895218: closest beacon is at x=3790542, y=2949630"));

    for i in helpers::smart_range(5, -4) {
        print!("{}, ", i);
    }
    println!();
    for i in 5..-4 {
        print!("{}, ", i);
    }
    println!();
    // let mut t = 0;
    // println!("Part 1: {}", t);
}
