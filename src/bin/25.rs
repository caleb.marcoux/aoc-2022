use std::fs;

fn snafu(input: &Vec<char>) -> isize {
    let mut total: isize = 0;
    for (n, num) in input.iter().rev().enumerate() {
        let val = match num {
            '=' => -2,
            '-' => -1,
            '0' => 0,
            '1' => 1,
            '2' => 2,
            _ => 0,
        };
        total += val * 5_isize.pow(n as u32);
    }
    return total;
}

fn main() {
    // Get input
    // let file_path = "25.example";
    let file_path = "25.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut values = Vec::new();

    // Parse input
    for (_, line) in lines.enumerate() {
        values.push(snafu(&line.chars().collect::<Vec<char>>()));
    }

    // Add values
    let mut t = 0;
    for val in values {
        t += val;
    }

    // Find SNAFU number
    let mut s = 0;
    let mut v: Vec<char> = Vec::new();
    let mut p = 0;
    // Find biggest place
    for n in 0.. {
        // Start bigger and subtract
        if s < t {
            s += 2 * 5_isize.pow(n as u32);
        } else {
            p = n;
            break;
        }
    }

    // Now move down powers to find correct digits
    s = 0;
    let mut r; // Remainder
    loop {
        // Too big.
        let n_pow = if p > 0 {5_isize.pow((p - 1) as u32)} else {0};
        if 2 * n_pow >= t {
            if p == 0 {
                break;
            }
            p -= 1;
            continue;
        }
        // Find correct digit
        r = t - s;
        let pow = 5_isize.pow(p as u32);
        if r > (pow + pow / 2) {
            s += 2 * pow;
            v.push('2');
        } else if r > 0 {
            s += pow;
            v.push('1');
        } else if r.abs() <= pow / 2 {
            v.push('0');
        } else if -r > (pow + pow / 2) {
            s -= 2 * pow;
            v.push('=');
        } else {
            s -= pow;
            v.push('-');
        }
        // Iterate power
        if p == 0 {
            break;
        }
        p -= 1;
    }

    // Check answer!
    println!("Back again: {}", snafu(&v));
    for val in v {
        print!("{}", val);
    }

    println!();
    println!("Part 1: {}", t);
}
