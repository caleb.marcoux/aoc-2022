use std::fs;

fn find_marker<const LENGTH: usize>(input: &Vec<usize>) {
    let mut counts = [0; 26];
    let mut duplicates = 0;
    for (i, c) in input.iter().enumerate() {
        // Add new character
        let new_pos: usize = *c;
        counts[new_pos] += 1;
        // Count up duplicate letters
        if counts[new_pos] == 2 {
            duplicates += 1;
        }

        if i >= LENGTH {
            // Remove old character
            let old_pos: usize = input[i - LENGTH];
            counts[old_pos] -= 1;
            // Count down duplicate letters
            if counts[old_pos] == 1 {
                duplicates -= 1;
            }
            // Check if we found a marker
            if duplicates == 0 {
                println!("{}", i + 1);
                return;
            }
        }
    }
    println!("No solution!");
}

fn main() {
    // Get input
    let file_path = "6.input";
    // Turn the input into a vector of numbers
    let input = fs::read_to_string(file_path)
        .expect("Could not read input")
        .chars().filter_map(|c| {
            // Handle null string end
            if c as usize > 'a' as usize {
                Some(c as usize - 'a' as usize)
            } else {
                None
            }
        })
        .collect::<Vec<usize>>();

    // Part 1
    find_marker::<4>(&input);
    // Part 2
    find_marker::<14>(&input);
}

