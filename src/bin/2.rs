use std::fs;

fn main() {
    // Get input
    let file_path = "2.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let rounds = input.split_terminator("\n");
    let mut score = 0;
    for round in rounds {
        // Get choice numbers
        let them = match round.chars().nth(0) {
            Some('A') => 1,
            Some('B') => 2,
            Some('C') => 3,
            _ => 0,
        };
        let you = match round.chars().nth(2) {
            Some('X') => (them + 1) % 3 + 1,
            Some('Y') => them,
            Some('Z') => them % 3 + 1,
            _ => 0,
        };
        // Base score
        score += you;
        // Outcome score
        // - draw
        if you == them {
            score += 3;
        }
        // - win
        if you == them % 3 + 1 {
            score += 6;
        }
    }
    println!("{}", score);
}
