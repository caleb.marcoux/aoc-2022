use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "7.example";
    let file_path = "7.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");

    let lines = input.split_terminator("\n");

    let mut path = Vec::new();
    let mut cur_dir = "";
    let mut ls = false;
    let mut sizes: HashMap<&str, u32> = HashMap::new();
    let mut dirs: HashMap<&str, HashSet<&str>> = HashMap::new();
    let mut finals: HashMap<&str, u32> = HashMap::new();
    for line in lines {
        let args = line.split_terminator(" ").collect::<Vec<&str>>();
        // let args = line.split_terminator(" ").collect::<Vec<String>>();
        // Command
        if args[0] == "$" {
            ls = false;
            if args[1] == "cd" {
                if args[2] == ".." {
                    path.pop();
                } else {
                    path.push(args[2]);
                }
                cur_dir = &*path.join("/");
                if !dirs.contains_key(&cur_dir) {
                    let new_dir = HashSet::new();
                    dirs.entry(cur_dir.clone()).or_insert(new_dir);
                }
                sizes.entry(cur_dir.clone()).or_insert(0);
            }
            if args[1] == "ls" {
                ls = true;
            }
        }
        // Sizes
        else if ls {
            if args[0] == "dir" {
                if !dirs.contains_key(&cur_dir) {
                    let new_dir = HashSet::new();
                    dirs.entry(cur_dir.clone()).or_insert(new_dir);
                }
                dirs.entry(cur_dir.clone()).and_modify(|d| {
                    d.insert(&*format!("{}/{}", cur_dir, args[1]));
                });
            } else {
                let s = sizes.entry(cur_dir.clone()).or_insert(0);
                *s += args[0].parse::<u32>().unwrap();
            }
        }
    }

    // Add up sizes
    while finals.len() < dirs.len() {
    // while finals.len() < 111 {
        // println!("{}:{}", finals.len(), dirs.len());
        for (dir, children) in &dirs {
            // No children
            if children.len() == 0 {
                finals.entry(dir).or_insert(*sizes.get(dir).unwrap());
            } else {
                // Sum size of subdirectories + self size.
                let mut children_ready = true;
                for c in children {
                    if !finals.contains_key(c) {
                        children_ready = false;
                    }
                }
                if children_ready {
                    let f = children.iter().map(|d| {
                        *finals.get(d).unwrap()
                    }).sum::<u32>() + *sizes.get(dir).unwrap();
                    finals.entry(dir).or_insert(f);
                }
            }
        }
    }

    // println!("dirs");
    // for (d, children) in &dirs {
    //     print!("\n{}.{}:", d, children.len());
    //     for c in children {
    //         print!("{}, ", c);
    //     }
    // }
    // println!("\nsizes");
    // for (d, s) in &sizes {
    //     println!("{}:{}", d, s);
    // }
    // println!("\nfinals");
    // for (d, s) in &finals {
    //     println!("{}:{}", d, s);
    // }

    // Get biggest
    let mut t = 0;
    let mut maxi = 0;
    let mut min_bigger = 70000000;
    for (_d, s) in finals {
        if s >= maxi {
            maxi = s;
        }
        if s <= 100000 {
            t += s;
        }
        if s >= 3629016 && s <= min_bigger {
            // println!("{}", s);
            min_bigger = s;
        }
    }
    println!("part 1:{}", t);
    let f = 70000000 - maxi;
    println!("free: {}", f);
    println!("need: {}", 30000000 - f);
    println!("min_bigger: {}", min_bigger);
}

