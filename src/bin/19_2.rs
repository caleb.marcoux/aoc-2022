use std::fs;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "19.example";
    let file_path = "19_2.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let blueprints = input.split_terminator("\n\n");
    let blueprints = input.split_terminator("\n");

    const TIME: usize = 32;
    let mut t = 1;

    for (id, blueprint) in blueprints.enumerate() {
        // Parse input
        let mut costs: [[usize; 3]; 4] = [[0; 3]; 4];
        for (i, line) in blueprint.split_terminator(['.', ':']).enumerate() {
            // Parse blueprint
            let term: Vec<&str> = line.trim().split_terminator(' ').collect();
            if i == 1 {
                // println!("blueprint: {}, {}, {}", term[3], term[4], term[5]);
                costs[0] = [term[4].parse().unwrap(), 0, 0];
            } else if i == 2 {
                costs[1] = [term[4].parse().unwrap(), 0, 0];
            } else if i == 3 {
                costs[2] = [term[4].parse().unwrap(), term[7].parse().unwrap(), 0];
            } else if i == 4 {
                costs[3] = [term[4].parse().unwrap(), 0, term[7].parse().unwrap()];
            } 
        }

        // Find best options.
        type Choice = ([usize; 4], [usize; 4], usize);
        let resources: [usize; 4] = [0; 4];
        let mut robots: [usize; 4] = [0; 4];
        robots[0] = 1; // Start 1 clay
        let mut choices: HashSet<Choice> = HashSet::new();
        choices.insert((resources, robots, 0));
        let mut best = 1;
        const THRESHOLD: usize = 50;
        let mut top = THRESHOLD;
        for m in 1..TIME + 1 {
            println!("Choices: {}:{} - {}", id, m, choices.len());
            let mut new_choices: HashSet<Choice> = HashSet::new();
            for choice in &choices {
                // Skip choices that are performing badly.
                if choice.2 > top {
                    top = choice.2;
                }
                if choice.2 < top - THRESHOLD {
                    continue;
                }
                // Valid choices from here.
                for (item, cost) in costs.iter().enumerate() {
                    let mut affordable = true;
                    for (i, material) in cost.iter().enumerate() {
                        if choice.0[i] < *material {
                            affordable = false;
                        }
                    }
                    // Not worth building if it will give us more resources per turn than we can
                    // build in that turn. ADDED AFTER COMP
                    let worth_it: bool = if item < 3 {
                        choice.0[item] < costs.iter().map(|c| c[item]).sum()
                    } else {
                        true // Always worth building geode robot
                    };
                    // Try building a robot
                    if affordable && worth_it {
                        // Spend resources
                        let mut new_resources = choice.0.clone();
                        for (i, material) in cost.iter().enumerate() {
                            new_resources[i] -= material;
                        }
                        // Get new robot
                        let mut new_robots = choice.1.clone();
                        new_robots[item] += 1;
                        // Add resources
                        let mut new_top = choice.2;
                        for (i, r) in new_resources.iter_mut().enumerate() {
                            new_top += new_robots[i] * (i + 1) * (i + 1); // Add robots
                            *r += choice.1[i]; // Past robots
                        }
                        new_choices.insert((new_resources, new_robots, new_top));
                    }
                }
                // Add do nothing choice
                let mut new_resources = choice.0.clone();
                // Add resources
                let mut new_top = choice.2;
                for (i, r) in new_resources.iter_mut().enumerate() {
                    new_top += choice.1[i] * (i + 1) * (i + 1); // Add robots
                    *r += choice.1[i]; // Past robots
                }
                new_choices.insert((new_resources, choice.1.clone(), new_top));
            }
            choices = new_choices;
        }

        // Check best choice
        for choice in choices {
            if choice.0[3] > best {
                best = choice.0[3];
            }
        }

        // Determine quality
        t *= best;
        println!("best: {}", best);
    }

    println!("Part 1: {}", t);
}
