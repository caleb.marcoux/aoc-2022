use std::fs;

fn find_marker(input: &str, window: usize) -> usize {
    let input = input.as_bytes();
    if input.len() < window {
        panic!("Input is too short: {}", input.len());
    }

    let mut seen = [0u32; 26];
    let mut uniq = 0;

    // Seed the algorithm with the first `window` bytes
    for idx in 0..window {
        let ch = (input[idx] - b'a') as usize;

        seen[ch] += 1;
        if seen[ch] == 1 {
            uniq += 1;
        }
    }

    // Fast path: check if the first `window` bytes are the solution
    if uniq == window {
        return window;
    }

    input
        .windows(window + 1)
        .enumerate()
        .find(|&(_idx, w)| {
            let ch = (w[0] - b'a') as usize;
            seen[ch] -= 1;
            if seen[ch] == 0 {
                uniq -= 1;
            }

            let ch = (w[window] - b'a') as usize;
            seen[ch] += 1;
            if seen[ch] == 1 {
                uniq += 1;
            }

            uniq == window
        })
        // convert the window index to character index
        .map(|(pos, w)| pos + w.len())
        .expect("no answer")
}

fn main() {
    // Get input
    let file_path = "6.input";
    // Turn the input into a vector of numbers
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");

    // Part 1
    println!("{}", find_marker(&input, 4));
    // Part 2
    println!("{}", find_marker(&input, 14));
}

