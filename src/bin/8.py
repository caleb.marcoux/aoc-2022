with open('8.input') as f:
    lines = f.read()
    rows = []
    for line in lines.split("\n"):
        rows.append(list(map(lambda c: int(c), list(line))))

    print(rows.pop())

    t = 0
    for i, row in enumerate(rows):
        # print(i)
        for k, h in enumerate(row):
            if len(row) == 0:
                continue
            # edge
            visible = False
            if k == 0 or k == len(row) - 1:
                visible = True
            if i == 0 or i == len(rows) - 1:
                visible = True
            if visible:
                t += 1
                continue
            # inner
            visible = True
            for j, n in enumerate(rows):
                if (j < i) and n[k] >= h:
                    visible = False
            if visible:
                t += 1
                continue
            visible = True
            for j, n in enumerate(row):
                if (j < k) and n >= h:
                    visible = False
            if visible:
                t += 1
                continue
            visible = True
            for j, n in enumerate(rows):
                if (j > i) and n[k] >= h:
                    visible = False
            if visible:
                t += 1
                continue
            visible = True
            for j, n in enumerate(row):
                if (j > k) and n >= h:
                    visible = False
            if visible:
                t += 1
                continue
    print(t)
