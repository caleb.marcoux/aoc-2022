use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::Chain;
use std::slice::Iter;

fn main() {
    // Get input
    // let file_path = "16.example";
    let file_path = "16.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines = input.split_terminator("\n");

    const MAX_TIME: usize = 26;

    let mut valves: HashMap<&str, usize> = HashMap::new();
    let mut children: HashMap<&str, Vec<&str>> = HashMap::new();
    let mut paths: HashMap<(&str, &str), usize> = HashMap::new();

    // Build valves network
    for (i, line) in lines.enumerate() {
        let sides: Vec<&str> = line.split_terminator("valve").collect();
        let terms: Vec<&str> = sides[0].split_terminator(' ').collect();
        let valve_name: &str = terms[1];
        let flow_rate: usize = terms[4].split_terminator(['=', ';']).collect::<Vec<&str>>()[1].parse().unwrap();
        let leads_to: Vec<&str> = sides[1].trim_start_matches("s ").trim().split_terminator(", ").collect();
        valves.insert(valve_name, flow_rate);
        children.insert(valve_name, leads_to.clone());
        // Set up initial paths
        if valve_name == "AA" {
            for to in leads_to {
                paths.insert((valve_name, to), 1);
            }
        }
    }

    // Make paths
    let mut found_dead = true;
    let mut i = 0;
    while found_dead && i < 10 {
        found_dead = false;
        let current_paths = paths.clone();
        for (path, cost) in current_paths {
            let (from, to) = path;
            let flow = valves.get(to).unwrap();
            // TODO: Hack solution!
            if cost > 9 {
                paths.remove(&path);
                continue;
            }
            if *flow == 0 {
                found_dead = true;
                // Add all child paths.
                for child in children.get(to).unwrap() {
                    let p = (from, *child);
                    let v = cost + 1;
                    if !paths.contains_key(&p) || v < *paths.get(&p).unwrap() {
                        paths.insert(p, v);
                    }
                }
                // Remove this path.
                paths.remove(&path);
            } else {
                // Don't skip any node
                for child in children.get(to).unwrap() {
                    let p = (to, *child);
                    let v = 1;
                    if !paths.contains_key(&p) || v < *paths.get(&p).unwrap() {
                        paths.insert(p, v);
                    }
                }
            }
        }
        // for (path, cost) in paths {
        //     let (from, to) = path;
        //     println!("({},{}) -> {}", from, to, cost);
        // }
        i += 1;
    }

    for (path, cost) in &paths {
        let (from, to) = path;
        println!("({},{}) -> {}", from, to, cost);
    }
    println!("# Paths: {}", paths.len());
    println!("# Valves: {}", valves.len());

    // Remake children
    children = HashMap::new();
    for ((from, to), _) in paths {
        if children.contains_key(from) {
            let mut new_vec = children.get(from).unwrap().clone();
            new_vec.push(to);
            children.insert(from, new_vec);
        } else {
            children.insert(from, vec![to]);
        }
    }

    for child in &children {
        println!("{}, {}", child.0, child.1.len());
    }
    println!("Children: {}", children.len());


    type Choice<'a> = (usize, &'a str, HashSet<&'a str>, &'a str);
    // type Choice<'a> = (u32, usize, &'a str);
    let mut choices: Vec<HashMap<(usize, &str, &str), Choice>> = vec![HashMap::new(); MAX_TIME];
    let first_choice = (0, "AA", HashSet::new(), "AA");
    choices[0].insert((0, "AA", "AA"), first_choice);

    const TIME_DELTA: usize = 1;

    // Simulate all possible choices
    'outer: for minute in 0..MAX_TIME {
        println!("{} - choices {}", minute, choices[minute].len());
        for (_, choice) in &choices[minute] {
            let i_stay = [choice.1];
            let e_stay = [choice.3];
            let valid: Chain<Iter<_>, Iter<_>> = children.get(choice.1).unwrap().iter().chain(i_stay.iter());
            let valid_e: Chain<Iter<_>, Iter<_>> = children.get(choice.3).unwrap().iter().chain(e_stay.iter());
            // We have opened everything.
            if choice.2.len() == children.len() {
                println!("Skipped");
                break 'outer;
            }
            // Travel down a path
            for v in valid {
                let this_valid_e = valid_e.clone();
                for e in this_valid_e {
                    // Start building new states
                    let mut new_release = choice.0;
                    let new_valve = *v; // Valve we moved to.
                    let new_e_valve = *e; // Elephant moved to.
                    let mut new_state = choice.2.clone(); // No new valves were opened (yet).
                    let new_choice;
                    // BOTH OPEN VALVE
                    if new_e_valve == choice.3 && new_valve == choice.1 {
                        // Open current valve
                        if !new_state.contains(new_valve) || !choice.2.contains(new_e_valve) {
                            new_release = choice.0;
                            if !new_state.contains(new_valve) {
                                new_release += &valves.get(new_valve).unwrap() * (MAX_TIME - minute - TIME_DELTA);
                                new_state.insert(new_valve);
                            }
                            if !new_state.contains(new_e_valve) {
                                new_release += &valves.get(new_e_valve).unwrap() * (MAX_TIME - minute - TIME_DELTA);
                                new_state.insert(new_e_valve);
                            }
                        }
                    }
                    // I OPEN VALVE
                    else if new_valve == choice.1 {
                        // Open current valve
                        new_release = choice.0;
                        if !new_state.contains(new_valve) {
                            new_release += &valves.get(new_valve).unwrap() * (MAX_TIME - minute - TIME_DELTA);
                            new_state.insert(new_valve);
                        }
                    }
                    // ELEPHANT OPENS VALVE
                    else if new_e_valve == choice.3 {
                        // Open current valve
                        new_release = choice.0;
                        if !new_state.contains(new_e_valve) {
                            new_release += &valves.get(new_e_valve).unwrap() * (MAX_TIME - minute - TIME_DELTA);
                            new_state.insert(new_e_valve);
                        }
                    }
                    new_choice = (new_release, new_valve, new_state, new_e_valve);
                    // Get time spent traveling.
                    // let new_time = minute + paths.get((choice))
                    // Me vs El in a position is equivalent.
                    if !choices[minute + 1].contains_key(&(new_release, new_valve, new_e_valve)) && !choices[minute + 1].contains_key(&(new_release, new_e_valve, new_valve)) {
                        choices[minute + 1].insert((new_release, new_valve, new_e_valve), new_choice);
                    }
                }
            }
        }
    }

    let mut m = 0;
    for (_, choice) in choices[MAX_TIME - 1] {
        if choice.0 > m {
            m = choice.0;
        }
    }

    println!("Part 2: {}", m);
}
