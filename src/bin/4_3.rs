use std::fs;

fn main() {
    // Get input
    let file_path = "4.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let pairs = input.split_terminator("\n");

    let mut total = 0;

    for pair in pairs {
        // Split pairs
        let (a, b, ..) = pair.split(',').collect::<Vec<&str>>();
        // let (a, b) = pair.chunks(',').next().unwrap();
        // let a = pair_tuple[0];
        // let b = pair_tuple[1];
        
        let a_pair: Vec<&str> = a.split('-').collect();
        let a_start = a_pair[0].parse::<i32>().unwrap();
        let a_end = a_pair[1].parse::<i32>().unwrap();
        let b_pair: Vec<&str> = b.split('-').collect();
        let b_start = b_pair[0].parse::<i32>().unwrap();
        let b_end = b_pair[1].parse::<i32>().unwrap();

        // Check which ranges cover
        if a_start <= b_start && a_end >= b_start {
            total += 1;
        } else if b_start <= a_start && b_end >= a_start {
            total += 1;
        }
    }

    println!("{}", total);
}

