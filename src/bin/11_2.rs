use std::fs;

fn main() {
    // Get input
    let file_path = "11.example";
    // let file_path = "11.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let starting = input.split_terminator("\n\n").collect::<Vec<&str>>();
    // let commands = input.split_terminator("\n\n");
    // let commands = input.split_terminator("\n").collect::<Vec<&str>>();
    
    // let mut monkeys: Vec<Vec<u64>> = Vec::new();
    let mut monkeys: Vec<Vec<Vec<Vec<u64>>>> = Vec::new();
    let mut inspected: Vec<u64> = vec![0; 8];

    // Build starting monkeys.
    for start in starting.iter() {
        for (i, line) in start.split("\n").enumerate() {
            let sides = line.split(": ").collect::<Vec<&str>>();
            if i == 1 {
                monkeys.push(sides[1].split(", ").map(|x| vec![vec![x.parse().unwrap()]]).collect::<Vec<Vec<Vec<u64>>>>());
            }
        }
    }

    // // Print the monkeys
    // for monkey in &monkeys {
    //     for item in monkey {
    //         print!("[");
    //         for term in item {
    //             for fac in term {
    //                 print!("{}*", *fac);
    //             }
    //             print!(" + ");
    //         }
    //         print!("]");
    //     }
    //     println!("");
    // }

    for _n in 0..1 {
        // println!("{}", n);
        for (j, start) in starting.iter().enumerate() {
            // Count items inspected
            inspected[j] += monkeys[j].len() as u64;
            // println!("{}", monkeys[j].len() as u64);
            // Monkey loop
            let mut divis = 0;
            let mut true_recip = 0;
            let mut false_recip: usize;
            // PRINT WORRY
            for monkey in &monkeys {
                for item in monkey {
                    print!("[");
                    for term in item {
                        for fac in term {
                            print!("{}*", *fac);
                        }
                        print!(" + ");
                    }
                    print!("]");
                }
                println!("");
            }
            // Monkey steps
            for (i, line) in start.split("\n").enumerate() {
                let sides = line.split(": ").collect::<Vec<&str>>();
                // WORRY LEVELS.
                if i == 2 {
                    let ops = sides[1].split(" ").collect::<Vec<&str>>();
                    for item in monkeys[j].iter_mut() {
                        if ops[4] == "old" {
                            let mut b = Vec::new();
                            let it = item.iter_mut();
                            for term in it {
                                b.append(term);
                            }
                            if ops[3] == "*" {
                                for fac in item.iter_mut() {
                                    fac.append(&mut b);
                                }
                            }
                        } else {
                            let b = ops[4].parse().unwrap();
                            if ops[3] == "+" {
                                item.push(vec![b]);
                            } else if ops[3] == "*" {
                                for fac in item.iter_mut() {
                                    fac.push(b);
                                }
                            }
                        }
                        // Assign new worry levels.
                        // AND DIVIDE!
                        // *item = ans;
                    }
                // TEST!
                } else if i == 3 {
                    divis = sides[1].split("divisible by ").collect::<Vec<&str>>()[1].parse::<u64>().unwrap();
                    // println!("Divis: {}", divis);
                } else if i == 4 {
                    true_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                } else if i == 5 {
                    false_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                    // TEST AND THROW ITEMS:
                    let items = monkeys[j].clone();
                    for item in items.iter() {
                        // Check if our fancy number is divisible.
                        let mut d = true;
                        for term in item {
                            let mut t = false;
                            for fac in term {
                                if fac % divis == 0 {
                                    t = true;
                                }
                            }
                            if !t {
                                d = false;
                            }
                        }
                        let mut simp = Vec::new();
                        for term in item {
                            let mut new_term = Vec::new();
                            for fac in term {
                                new_term.push(*fac);
                            }
                            simp.push(new_term);
                        }
                        if d {
                            monkeys[true_recip].push(simp);
                        } else {
                            monkeys[false_recip].push(simp);
                        }
                    }
                    // Remove items from self
                    monkeys[j] = Vec::new();
                }
            }
        }
    }

    for insp in &inspected {
        println!("{}", insp);
    }
    inspected.sort();
    println!("Part 1: {}", inspected[inspected.len()-1] * inspected[inspected.len()-2]);
}
