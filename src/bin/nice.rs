let [a, b, ..]: Vec<&str> = pair.split(',').collect();
let [a_start, a_end, ..]: Vec<&str> = a.split('-').collect();
let [b_start, b_end, ..]: Vec<&str> = b.split('-').collect();
