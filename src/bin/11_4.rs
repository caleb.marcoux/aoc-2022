use std::fs;

fn main() {
    // Get input
    // let file_path = "11.example";
    let file_path = "11.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let starting = input.split_terminator("\n\n").collect::<Vec<&str>>();
    
    let mut monkeys: Vec<Vec<u64>> = Vec::new();
    let mut inspected: Vec<u64> = vec![0; 8];

    // Build starting monkeys.
    for start in starting.iter() {
        for (i, line) in start.split("\n").enumerate() {
            let sides = line.split(": ").collect::<Vec<&str>>();
            if i == 1 {
                monkeys.push(sides[1].split(", ").map(|x| x.parse().unwrap()).collect::<Vec<u64>>());
            }
        }
    }

    for _n in 0..10000 {
        // println!("{}", n);
        for (j, start) in starting.iter().enumerate() {
            // Count items inspected
            inspected[j] += monkeys[j].len() as u64;
            // Monkey loop
            let mut divis = 0;
            let mut true_recip = 0;
            let mut false_recip: usize;
            // Monkey steps
            for (i, line) in start.split("\n").enumerate() {
                let sides = line.split(": ").collect::<Vec<&str>>();
                // WORRY LEVELS.
                if i == 2 {
                    let ops = sides[1].split(" ").collect::<Vec<&str>>();
                    for item in monkeys[j].iter_mut() {
                        let a: u64 = *item;
                        let mut b: u64 = *item;
                        let mut ans: u64 = 0;
                        if ops[4] != "old" {
                            b = ops[4].parse().unwrap();
                        }
                        if ops[3] == "+" {
                            ans = a + b;
                        } else if ops[3] == "-" {
                            ans = a - b;
                        } else if ops[3] == "*" {
                            ans = a * b;
                        }
                        // Assign new worry levels.
                        // AND DIVIDE!
                        *item = ans % 9699690;
                    }
                // TEST!
                } else if i == 3 {
                    divis = sides[1].split("divisible by ").collect::<Vec<&str>>()[1].parse().unwrap();
                    // println!("Divis: {}", divis);
                } else if i == 4 {
                    true_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                } else if i == 5 {
                    false_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                    // TEST AND THROW ITEMS:
                    let items = monkeys[j].clone();
                    for item in items.iter() {
                        if *item % divis == 0 {
                            monkeys[true_recip].push(*item);
                        } else {
                            monkeys[false_recip].push(*item);
                        }
                    }
                    // Remove items from self
                    monkeys[j] = Vec::new();
                }
            }
        }
    }

    for insp in &inspected {
        println!("{}", insp);
    }
    inspected.sort();
    println!("Part 2: {}", inspected[inspected.len()-1] * inspected[inspected.len()-2]);
}
