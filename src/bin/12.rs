use std::fs;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "12.example";
    let file_path = "12.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let starting = input.split_terminator("\n\n").collect::<Vec<&str>>();
    let rows = input.split_terminator("\n");

    let mut grid: Vec<Vec<usize>> = Vec::new();
    let mut start = (0, 0);
    let mut end = (0, 0);

    // Create map
    let mut i: usize = 0;
    let mut j: usize;
    for row in rows {
        j = 0;
        grid.push(row.chars().map(|x| {
            j += 1;
            if x == 'S' {
                // Set start point
                start = (i, j - 1);
                return 0
            } else if x == 'E' {
                // Set end point
                end = (i, j - 1);
                return 25
            } else {
                return x as usize - 'a' as usize
            }
        }).collect::<Vec<usize>>());
        i += 1;
    }

    // Print the grid
    for row in &grid {
        for col in row {
            print!("{}, ", *col);
        }
        println!("");
    }
    // Print start/end
    println!("Start: {}, {}", start.0, start.1);
    println!("End: {}, {}", end.0, end.1);

    // Start search
    let mut points = vec![(vec![start], start)];
    let mut explored = HashSet::new();
    explored.insert(start);
    let mut length: usize = 0;
    let mut found = false;
    while !found {
        // let p_iter = points.clone();
        // println!("Round");
        let mut new_points = Vec::new();
        for point in points {
            let p = point.1;
            // print!("({}, {}), ", p.0, p.1);
            let path = &point.0;
            // Get available moves.
            let mut moves = Vec::new();
            if p.0 > 0 {
                moves.push((p.0 - 1, p.1));
            }
            if p.1 > 0 {
                moves.push((p.0, p.1 - 1));
            }
            if p.0 < grid.len() - 1 {
                moves.push((p.0 + 1, p.1));
            }
            if p.1 < grid[0].len() - 1 {
                moves.push((p.0, p.1 + 1));
            }
            let valid = moves.iter().filter(|m| {
                return grid[m.0][m.1] <= grid[p.0][p.1] + 1 && !explored.contains(m);
            }).collect::<Vec<_>>();
            // Spawn children for each.
            // let mut dead = true;
            for v in valid {
                if *v == end {
                    length = path.len();
                    found = true;
                    break;
                }
                let mut new_path = path.clone();
                new_path.push(*v);
                new_points.push((new_path, *v));
                explored.insert(*v);
            }
            // Print data.
            // for m in valid {
            //     print!("({}, {}), ", m.0, m.1);
            // }
            // println!("Valid:");
            // println!("points: {}", points.len());
        }
        points = new_points;
    }
    println!("success!");
    println!("Length of path: {}", length);
}
