# Group numbers
f = open('1.input')
string_input = f.read()
for group in string_input.split('\n\n'):
    # Split the group into numbers
    print('n')
    print(sum(map(lambda e: int(e), group.split('\n'))))
# Sum each group
# Find highest number
