use std::fs;
use std::cmp;

fn main() {
    // Get input
    // let file_path = "14.example";
    let file_path = "14.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let paths = input.split_terminator("\n");

    const WIDTH: usize = 500;
    const HEIGHT: usize = 250;
    const X_OFF: usize = 250;
    // Sand pours from 500,0
    let mut cave: [[bool; WIDTH]; HEIGHT] = [[false; WIDTH]; HEIGHT];

    // Make the cave
    for path in paths {
        let coords = path.split_terminator(" -> ");
        let mut prev_x = 0;
        let mut prev_y = 0;
        for (i, coord) in coords.enumerate() {
            let vals: Vec<usize> = coord.split_terminator(",").map(|x| x.parse().unwrap()).collect();
            let cur_x: usize = vals[0] - X_OFF;
            let cur_y: usize = vals[1];
            // Skip first coord
            if i == 0 {
                prev_x = cur_x;
                prev_y = cur_y;
                continue;
            }
            // VERTICAL LINE
            if prev_x == cur_x {
                for y in cmp::min(cur_y, prev_y)..cmp::max(cur_y, prev_y) + 1 {
                    cave[y][cur_x] = true;
                }
            }
            // HORIZONTAL LINE
            if prev_y == cur_y {
                for x in cmp::min(cur_x, prev_x)..cmp::max(cur_x, prev_x) + 1 {
                    cave[cur_y][x] = true;
                }
            }
            prev_x = cur_x;
            prev_y = cur_y;
        }
    }

    // Simulate sand
    let mut i = 0;
    let mut full = false;
    while !full {
        // Loop through first piece of sand.
        let mut x = 500 - X_OFF;
        let mut y = 0;
        loop {
            // Check if sand off screen
            if y + 1 >= cave.len() {
                full = true;
                break;
            }
            // Move sand
            if !cave[y + 1][x] {
                y += 1;
            } else if !cave[y + 1][x - 1] {
                y += 1;
                x -= 1;
            } else if !cave[y + 1][x + 1] {
                y += 1;
                x += 1;
            } else {
                // At rest
                cave[y][x] = true;
                break;
            }
        }
        i += 1;
    }

    // print cave
    for row in cave {
        for col in row {
            print!("{}", match col {
                false => ".",
                true => "o",
            });
        }
        println!("");
    }

    println!("Part 1: {}", i - 1);
}
