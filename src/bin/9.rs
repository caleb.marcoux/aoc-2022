use std::fs;
use std::collections::HashSet;

fn simulate<const LENGTH: usize>(moves: &Vec<&str>) -> usize {
    let mut r: [[i32; 2]; LENGTH] = [[0, 0]; LENGTH];
    let mut p: HashSet<(i32, i32)> = HashSet::new();

    for m in moves {
        let dir = m.chars().nth(0).unwrap();
        let a: [i32; 2] = match dir {
            'U' => [0, -1],
            'D' => [0, 1],
            'L' => [-1, 0],
            'R' => [1, 0],
            _ => [0, 0]
        };
        let n: u32 = m[2..].parse().unwrap();
        for _i in 0..n {
            // Record the tail position.
            p.insert((r[LENGTH - 1][0], r[LENGTH - 1][1]));
            // Apply the move to head
            r[0][0] += a[0];
            r[0][1] += a[1];
            // Calculate the rope movement
            for j in 1..LENGTH {
                // Calculate the knot movement
                let mut b: [i32; 2] = [0, 0];
                let d: [i32; 2] = [r[j - 1][0] - r[j][0], r[j - 1][1] - r[j][1]];
                if d[0].abs() > 1 || d[1].abs() > 1 {
                    if d[0] > 0 {
                        b[0] = 1;
                    }
                    if d[1] > 0 {
                        b[1] = 1;
                    }
                    if d[0] < 0 {
                        b[0] = -1;
                    }
                    if d[1] < 0 {
                        b[1] = -1;
                    }
                }
                // Apply the knot movement
                r[j][0] += b[0];
                r[j][1] += b[1];
            }
        }
    }
    p.insert((r[LENGTH - 1][0], r[LENGTH - 1][1]));
    return p.len();
}

fn main() {
    // Get input
    // let file_path = "9.example";
    // let file_path = "9_2.example";
    let file_path = "9.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let moves = input.split_terminator("\n").collect();

    println!("Part 1: {}", simulate::<2>(&moves));
    println!("Part 2: {}", simulate::<10>(&moves));
}
