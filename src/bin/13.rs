use std::fs;
use std::cmp;

fn main() {
    // Get input
    // let file_path = "13.example";
    let file_path = "13.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let pairs = input.split_terminator("\n\n");

    let mut i = 1;
    let mut t = 0;
    let mut tc = 0;
    for pair in pairs {
        let sides: Vec<&str> = pair.split_terminator("\n").collect();
        let left = sides[0];
        let right = sides[1];
        // Check if the values are in order.
        let mut lo: isize = 0;
        let mut ro: isize = 0;
        let mut wrong = false;
        println!("Pair: {}", i);
        for c in 0..cmp::min(left.len(), right.len()) {
            let lc = (c as isize + lo) as usize;
            let rc = (c as isize + ro) as usize;
            // Left ends first.
            if lc >= left.len() {
                // All good
                println!("LEFT ENDS FIRST");
                break;
            }
            // Right ends first.
            if rc >= right.len() {
                println!("RIGHT ENDS FIRST");
                t += i;
                wrong = true;
                break;
            }
            let l = left.chars().nth(lc).unwrap();
            let r = right.chars().nth(rc).unwrap();
            println!("{}: {}", sides[0], l);
            println!("{}: {}", sides[1], r);
            if r == l {
                if r == ',' {
                    if ro < 0 {
                        // Num turned to array, so array must be ended.
                        println!("RIGHT ENDS FIRST (tras to array)");
                        t += i;
                        wrong = true;
                        break;
                    }
                    if lo < 0 {
                        println!("LEFT ENDS FIRST (tras to array)");
                        break;
                    }
                }
                if l == '1' {
                    let mut r_single_digit = true;
                    let mut l_single_digit = true;
                    if right.chars().nth(rc + 1).unwrap() != ',' && right.chars().nth(rc + 1).unwrap() != ']' {
                        // ro += 1;
                        r_single_digit = false;
                        println!("ADDED");
                    }
                    if left.chars().nth(lc + 1).unwrap() != ',' && left.chars().nth(lc + 1).unwrap() != ']' {
                        // lo += 1;
                        l_single_digit = false;
                        println!("ADDED");
                    }
                    if r_single_digit && !l_single_digit {
                        println!("Wrong Order: {}, {}", l, r);
                        t += i;
                        wrong = true;
                        break;
                    } else if r_single_digit && l_single_digit {
                        // These match
                        continue;
                    } else {
                        println!("Correct Order: {}, {}", l, r);
                        // tc += 1;
                        break;
                    }
                }
                // SAME
            } else {
                // NOT SAME
                if l == '[' {
                    ro -= 1;
                    // continue;
                }
                if r == '[' {
                    lo -= 1;
                    // continue;
                }
                if r == ']' {
                    // Right ends first
                    println!("RIGHT ENDS FIRST");
                    t += i;
                    wrong = true;
                    break;
                } else if l == ']' {
                    println!("LEFT ENDS FIRST");
                    break;
                } else if l == '[' || r == '[' || r == ',' || l == ',' {
                    // Do nothing
                } else {
                    // Numbers
                    let mut r_single_digit = true;
                    let mut l_single_digit = true;
                    if right.chars().nth(rc + 1).unwrap() != ',' && right.chars().nth(rc + 1).unwrap() != ']' {
                        r_single_digit = false;
                        println!("RIGHT");
                    }
                    if left.chars().nth(lc + 1).unwrap() != ',' && left.chars().nth(lc + 1).unwrap() != ']' {
                        l_single_digit = false;
                        println!("LEFT");
                    }
                    // Wrong order
                    if (r_single_digit && l_single_digit && l as usize > r as usize) || (r_single_digit && !l_single_digit) {
                        println!("Wrong Order: {}, {}", l, r);
                        t += i;
                        wrong = true;
                        break;
                    } else {
                        println!("Correct Order: {}, {}", l, r);
                        // tc += 1;
                        break;
                    }
                }
            }
        }
        if !wrong {
            tc += i;
        }
        i += 1;
    }
    println!("Wrong: {}", t);
    println!("Part 1: {}", tc);
}
