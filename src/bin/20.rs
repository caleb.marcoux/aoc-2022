use std::fs;

fn main() {
    // Get input
    let file_path = "20.example";
    // let file_path = "20.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // const KEY: isize = 811589153;
    const KEY: isize = 0;
    let numbers: Vec<isize> = input.split_terminator("\n").map(|x| x.parse::<isize>().unwrap() * KEY).collect();
    // Keep track of the old idices so we can find the number later.
    let mut mixed: Vec<(usize, isize)> = Vec::new();
    for (i, number) in numbers.iter().enumerate() {
        mixed.push((i, *number));
    }

    for _n in 0..10 {
        let mut mix_i = 0;
        for (i, number) in numbers.iter().enumerate() {
            // Skip zeros
            if *number == 0 {
                continue;
            }
            // Find the current position of number
            let mut mixed_number = None;
            for (j, mix) in mixed.iter().enumerate() {
                if mix.0 == i {
                    mixed_number = Some(*mix);
                    mix_i = j as isize;
                    break;
                }
            }
            println!("i:num {}: {}", i, number);
            // Remove from the old position
            mixed.remove(mix_i as usize);
            // Find the new position for the number
            let num_len = numbers.len() as isize;
            let offset = if *number < 0 {
                mix_i + number
            } else {
                mix_i + number + 1
            };
            let new_pos = offset.rem_euclid(num_len);
            println!("pos:mix_i {}: {}", new_pos, mix_i);
            // Insert into the new position
            mixed.insert(new_pos as usize, mixed_number.unwrap());
            if new_pos < mix_i {
                mix_i += 1;
            }
            // Print iteration
        }
        for mix in &mixed {
            print!("{}, ", mix.1);
        }
        println!();
    }

    // Find 0 position
    let mut zero_i = 0;
    for (i, num) in mixed.iter().enumerate() {
        if num.1 == 0 {
            zero_i = i;
            break;
        }
    }

    // 13148
    // -15909
    println!("{}", mixed[(zero_i + 1000) % mixed.len()].1);
    println!("{}", mixed[(zero_i + 2000) % mixed.len()].1);
    println!("{}", mixed[(zero_i + 3000) % mixed.len()].1);
    let t = mixed[(zero_i + 1000) % mixed.len()].1 + mixed[(zero_i + 2000) % mixed.len()].1 + mixed[(zero_i + 3000) % mixed.len()].1;
    println!("Part 1: {}", t);
}
