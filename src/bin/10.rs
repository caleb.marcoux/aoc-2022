use std::fs;

fn main() {
    // Get input
    let file_path = "10.example";
    // let file_path = "10.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let commands = input.split_terminator("\n").collect::<Vec<&str>>();

    let mut x = 1;
    let mut cycle = 0;
    let mut t = 0;

    for command in commands.iter() {
        if *command == "noop" {
            cycle += 1;
            if cycle % 40  >= x - 1 && cycle % 40 <= x + 1 {
                print!("#");
            } else {
                print!(".");
            }
            if cycle % 40 == 0 {
                // Start new row.
                print!("\n");
            }
            if cycle == 20 || (cycle - 20) % 40 == 0 {
                let strength = x * cycle;
                // println!("{}", strength);
                t += strength;
            }
            continue;
        } else {
            cycle += 1;
            if cycle % 40 >= x - 1 && cycle % 40 <= x + 1 {
                print!("#");
            } else {
                print!(".");
            }
            if cycle % 40 == 0 {
                // Start new row.
                print!("\n");
            }
            if cycle == 20 || (cycle - 20) % 40 == 0 {
                let strength = x * cycle;
                // println!("{}", strength);
                t += strength;
            }
            cycle += 1;
            if cycle % 40 >= x - 1 && cycle % 40 <= x + 1 {
                print!("#");
            } else {
                print!(".");
            }
            if cycle % 40 == 0 {
                // Start new row.
                print!("\n");
            }
            if cycle == 20 || (cycle - 20) % 40 == 0 {
                let strength = x * cycle;
                // println!("{}", strength);
                t += strength;
            }
            let v = command.split(" ").nth(1).unwrap().parse::<i32>().unwrap();
            x += v;
        }
    }

    println!("Part 1: {}", t);
}
