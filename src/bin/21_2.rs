use std::fs;
use std::collections::HashMap;

fn main() {
    // Get input
    // let file_path = "21.example";
    let file_path = "21.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines = input.split_terminator("\n");
    let mut monkeys: HashMap<&str, isize> = HashMap::new();
    let mut exprs: HashMap<&str, (&str, &str, &str)> = HashMap::new();

    for line in lines {
        // Parse input
        let terms: Vec<&str> = line.split_terminator(": ").collect();
        let try_num = terms[1].parse::<isize>();
        match try_num {
            Ok(num) => {
                monkeys.insert(terms[0], num);
            },
            Err(_) => {
                let ops: Vec<&str> = terms[1].split(' ').collect();
                exprs.insert(terms[0], (ops[0], ops[1], ops[2]));
            },
        };
    }

    monkeys.remove("humn");

    // Loop until we find one side of the root equation
    let mut prev_len = monkeys.len();
    'filling: loop {
        for (name, expr) in &exprs {
            if monkeys.contains_key(expr.0) && monkeys.contains_key(expr.2) && *name != "humn" {
                let a = *monkeys.get(expr.0).unwrap();
                let b = *monkeys.get(expr.2).unwrap();
                let num = match expr.1 {
                    "+" => a + b,
                    "*" => a * b,
                    "-" => a - b,
                    "/" => a / b,
                    _ => 0,
                };
                monkeys.insert(name, num);
                if *name == "lrnp" {
                    monkeys.insert("ptnb", num);
                }
                if *name == "ptnb" {
                    monkeys.insert("lrnp", num);
                }
            }
        }
        if monkeys.len() == prev_len {
            break 'filling;
        }
        prev_len = monkeys.len();
    }

    println!("Finished filling");

    // Loop backward until we find humn.
    'backward: loop {
        for (name, expr) in &exprs {
            // This is a term we want to find
            // if finding.contains(expr.0) || finding.contains(expr.2) {
            if monkeys.contains_key(name) && (monkeys.contains_key(expr.0) || monkeys.contains_key(expr.2)) && !(monkeys.contains_key(expr.0) && monkeys.contains_key(expr.2)) {
                let known = if monkeys.contains_key(expr.0) {
                    expr.0
                } else {
                    expr.2
                };
                let other = if monkeys.contains_key(expr.2) {
                    expr.0
                } else {
                    expr.2
                };
                let k = *monkeys.get(known).unwrap();
                let p = *monkeys.get(name).unwrap();
                let num = if known == expr.2 {
                    match expr.1 {
                        "-" => p + k,
                        "/" => p * k,
                        "+" => p - k,
                        "*" => p / k,
                        _ => 0,
                    }
                } else {
                    match expr.1 {
                        "-" => k - p,
                        "/" => k / p,
                        "+" => p - k,
                        "*" => p / k,
                        _ => 0,
                    }
                };
                monkeys.insert(other, num);
                // finding.remove(other);
                if other == "humn" {
                    break 'backward;
                }
            }
        }
    }

    let humn = monkeys.get("humn").unwrap();
    println!("Part 2: {}", humn);
}
