use std::fs;

struct Elf {
    id: i32,
    total: i32,
    // cals: vec!,
}

fn main() {
    // Get input calories/build elf
    let file_path = "1.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // print!("{}", input);
    let elves = input.split("\n\n");
    let mut cals = elves.enumerate().map(|(i, e)| {
        // Get sum of elf calories.
        let t = e.split_terminator("\n").map(|v| v.parse::<i32>().unwrap()).sum();
        // Return new Elf
        Elf {
            id: i as i32,
            total: t,
        }
    }).collect::<Vec<Elf>>();
    cals.sort_unstable_by(
        |a, b| b.total.cmp(&a.total));
    // Sort elf by total calories
    // Get highest calory elf.
    println!("{}", cals[0].total + cals[1].total + cals[2].total);
}
