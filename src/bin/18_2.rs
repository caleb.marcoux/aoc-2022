use std::fs;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "18.example";
    let file_path = "18.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines: Vec<&str> = input.split_terminator('\n').collect();

    type Cube = (i32, i32, i32);
    let mut cubes: HashSet<Cube> = HashSet::new();
    let mut spaces: HashSet<Cube> = HashSet::new();

    for line in lines {
        let coords: Vec<i32> = line.split_terminator(',')
            .map(|c| c.parse().unwrap()).collect();
        let x = coords[0];
        let y = coords[1];
        let z = coords[2];
        cubes.insert((x, y, z));
    }

    let mut t = 0;
    for cube in &cubes {
        // whether each face is open
        let mut free = 0;
        let mut x_b = true;
        let mut y_b = true;
        let mut z_b = true;
        let mut x_f = true;
        let mut y_f = true;
        let mut z_f = true;
        for other in &cubes {
            if cube != other {
                // Check if side is covered
                if cube.0 == other.0 + 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_f = false;
                }
                if cube.0 == other.0 - 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_b = false;
                }
                if cube.1 == other.1 + 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_f = false;
                }
                if cube.1 == other.1 - 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_b = false;
                }
                if cube.2 == other.2 + 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_f = false;
                }
                if cube.2 == other.2 - 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_b = false;
                }
            }
        }
        // Add up free sides
        if x_f {
            free += 1;
            spaces.insert((cube.0 - 1, cube.1, cube.2));
        }
        if x_b {
            free += 1;
            spaces.insert((cube.0 + 1, cube.1, cube.2));
        }
        if y_f {
            free += 1;
            spaces.insert((cube.0, cube.1 - 1, cube.2));
        }
        if y_b {
            free += 1;
            spaces.insert((cube.0, cube.1 + 1, cube.2));
        }
        if z_f {
            free += 1;
            spaces.insert((cube.0, cube.1, cube.2 - 1));
        }
        if z_b {
            free += 1;
            spaces.insert((cube.0, cube.1, cube.2 + 1));
        }
        t += free;
    }

    // Start searching from all spaces
    let mut added: HashSet<Cube> = HashSet::new();
    'checking: for space in &spaces {
        if added.contains(space) {
            continue 'checking;
        }
        let mut open: HashSet<Cube> = HashSet::new();
        open.insert(*space);
        let mut explored: HashSet<Cube> = HashSet::new();
        let mut last_explored: isize = -1;
        while open.len() > 0 && explored.len() as isize != last_explored {
            last_explored = explored.len() as isize;
            let mut new_open = HashSet::new();
            for o in &open {
                if !cubes.contains(o) && !explored.contains(o) {
                    // Add posibilities
                    for d in [(0, 0, 1), (0, 0, -1), (0, 1, 0), (0, -1, 0), (1, 0, 0), (-1, 0, 0)] {
                        new_open.insert((o.0 + d.0, o.1 + d.1, o.2 + d.2));
                    }
                    explored.insert(*o);
                }
                if o.0 < 0 || o.1 < 0 || o.2 < 0 {
                    // We must be in the open
                    // println!("CONTINUED");
                    continue 'checking;
                }
            }
            open = new_open;
        }
        // println!("ADDED: {}, {}, {}", space.0, space.1, space.2);
        added.insert(*space);
        // if !cubes.contains(space) {
        //     for e in explored {
        //         println!("ADDED: {}, {}, {}", e.0, e.1, e.2);
        //         added.insert(e);
        //     }
        // }
    }

    // Subtrack added cubes.
    for cube in &added {
        // println!("Cube: ({},{},{})", cube.0, cube.1, cube.2);
        // whether each face is open
        let mut free = 0;
        let mut x_b = true;
        let mut y_b = true;
        let mut z_b = true;
        let mut x_f = true;
        let mut y_f = true;
        let mut z_f = true;
        for other in &cubes {
        // println!("Other: ({},{},{})", other.0, other.1, other.2);
            if cube != other {
                // Check if side is covered
                if cube.0 == other.0 + 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_f = false;
                }
                if other.0 > 0 && cube.0 == other.0 - 1 && cube.1 == other.1 && cube.2 == other.2 {
                    x_b = false;
                }
                if cube.1 == other.1 + 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_f = false;
                }
                if other.1 > 0 && cube.1 == other.1 - 1 && cube.0 == other.0 && cube.2 == other.2 {
                    y_b = false;
                }
                if cube.2 == other.2 + 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_f = false;
                }
                if other.2 > 0 && cube.2 == other.2 - 1 && cube.1 == other.1 && cube.0 == other.0 {
                    z_b = false;
                }
            }
        }
        // Add up free sides
        if x_f {
            free += 1;
        }
        if x_b {
            free += 1;
        }
        if y_f {
            free += 1;
        }
        if y_b {
            free += 1;
        }
        if z_f {
            free += 1;
        }
        if z_b {
            free += 1;
        }
        // println!("Inner sides: {}", free);
        t -= 6 - free;
    }

    println!("ADDED: {}", added.len());
    println!("Part 1: {}", t);
}
