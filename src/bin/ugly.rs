let pair_tuple: Vec<&str> = pair.split(',').collect();
let a = pair_tuple[0];
let b = pair_tuple[1];

let a_pair: Vec<&str> = a.split('-').collect();
let a_start = a_pair[0].parse::<i32>().unwrap();
let a_end = a_pair[1].parse::<i32>().unwrap();
let b_pair: Vec<&str> = b.split('-').collect();
let b_start = b_pair[0].parse::<i32>().unwrap();
let b_end = b_pair[1].parse::<i32>().unwrap();
