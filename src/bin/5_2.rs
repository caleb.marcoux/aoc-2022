use std::fs;

fn main() {
    // Setup stacks
    let mut stacks: [Vec<char>; 10] = [
        vec![], // element 0 not used
        vec!['S', 'C', 'V', 'N'],
        vec!['Z', 'M', 'J', 'H', 'N', 'S'],
        vec!['M', 'C', 'T', 'G', 'J', 'N', 'D'],
        vec!['T', 'D', 'F', 'J', 'W', 'R', 'M'],
        vec!['P', 'F', 'H'],
        vec!['C', 'T', 'Z', 'H', 'J'],
        vec!['D', 'P', 'R', 'Q', 'F', 'S', 'L', 'Z'],
        vec!['C', 'S', 'L', 'H', 'D', 'F', 'P', 'W'],
        vec!['D', 'S', 'M', 'P', 'F', 'N', 'G', 'Z'],
    ];

    // Get input
    let file_path = "5.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let moves = input.split_terminator("\n");
    for mover in moves {
        let steps: Vec<&str> = mover.split_terminator(' ').collect();
        // Move
        let to_stack: usize = steps[5].parse().unwrap();
        let from_stack: usize = steps[3].parse().unwrap();
        let n: usize = steps[1].parse().unwrap();
        let len = stacks[from_stack].len().saturating_sub(n);
        let mut items: Vec<char> = stacks[from_stack].split_off(len);
        stacks[to_stack].append(&mut items);
    }

    // Print stack tops
    for s in stacks {
        if s.len() > 0 {
            print!("{}", s.last().unwrap());
        }
    }
    println!("");
}

