use std::fs;
use std::cmp;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "15.example";
    // let y=10;
    let file_path = "15.input";
    let y=2000000;
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines = input.split_terminator("\n");

    type Point = (i32, i32);
    let mut sensors: HashSet<(Point, Point)> = HashSet::new();
    let mut beacons: HashSet<Point> = HashSet::new();

    for line in lines {
        let terms: Vec<&str> = line.split_terminator(['=', ',', ':']).collect();
        let s_x: i32 = terms[1].parse().unwrap();
        let s_y: i32 = terms[3].parse().unwrap();
        let b_x: i32 = terms[5].parse().unwrap();
        let b_y: i32 = terms[7].parse().unwrap();
        sensors.insert(((s_x, s_y), (b_x, b_y)));
        beacons.insert((b_x, b_y));
    }

    type Range = (i32, i32);
    let mut ranges: HashSet<Range> = HashSet::new();
    let mut covered: HashSet<Point> = HashSet::new();

    // Find empty spaces
    let mut c = 0;
    for sensor in &sensors {
        let dist = (sensor.0.0 - sensor.1.0).abs() + (sensor.0.1 - sensor.1.1).abs();

        // Use ranges
        let y_diff = (sensor.0.1 - y).abs();
        ranges.insert((sensor.0.0 - (dist - y_diff), sensor.0.0 + (dist - y_diff)));
    }

    // Loop over ranges
    for range in ranges {
        for n in range.0..range.1 + 1 {
            covered.insert((n, y));
        }
    }

    // Remove beacons
    for beacon in beacons {
        if beacon.1 == y {
            c -= 1;
        }
    }

    // find coverage
    for cov in covered {
        if cov.1 == y {
            c += 1;
        }
    }

    println!("Part 1: {}", c);
}
