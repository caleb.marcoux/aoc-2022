use std::fs;
use std::cmp;
use std::cmp::Ordering;

fn main() {
    // Get input
    // let file_path = "13.example";
    let file_path = "13.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let pairs = input.split_terminator("\n\n");

    let mut packets: Vec<&str> = Vec::new();
    for pair in pairs {
        let sides: Vec<&str> = pair.split_terminator("\n").collect();
        let left = sides[0];
        let right = sides[1];
        packets.push(left);
        packets.push(right);
    }
    packets.push("[[2]]");
    packets.push("[[6]]");
    // Sort the packets.
    packets.sort_unstable_by(|left, right| {
        // Check if the values are in order.
        let mut lo: isize = 0;
        let mut ro: isize = 0;
        let mut wrong = false;
        for c in 0..cmp::min(left.len(), right.len()) {
            let lc = (c as isize + lo) as usize;
            let rc = (c as isize + ro) as usize;
            // Left ends first.
            if lc >= left.len() {
                // All good
                break;
            }
            // Right ends first.
            if rc >= right.len() {
                wrong = true;
                break;
            }
            let l = left.chars().nth(lc).unwrap();
            let r = right.chars().nth(rc).unwrap();
            if r == l {
                if r == ',' {
                    if ro < 0 {
                        // Num turned to array, so array must be ended.
                        wrong = true;
                        break;
                    }
                    if lo < 0 {
                        break;
                    }
                }
                if l == '1' {
                    let mut r_single_digit = true;
                    let mut l_single_digit = true;
                    if right.chars().nth(rc + 1).unwrap() != ',' && right.chars().nth(rc + 1).unwrap() != ']' {
                        // ro += 1;
                        r_single_digit = false;
                    }
                    if left.chars().nth(lc + 1).unwrap() != ',' && left.chars().nth(lc + 1).unwrap() != ']' {
                        // lo += 1;
                        l_single_digit = false;
                    }
                    if r_single_digit && !l_single_digit {
                        wrong = true;
                        break;
                    } else if r_single_digit && l_single_digit {
                        // These match
                        continue;
                    } else {
                        // tc += 1;
                        break;
                    }
                }
                // SAME
            } else {
                // NOT SAME
                if l == '[' {
                    ro -= 1;
                    // continue;
                }
                if r == '[' {
                    lo -= 1;
                    // continue;
                }
                if r == ']' {
                    // Right ends first
                    wrong = true;
                    break;
                } else if l == ']' {
                    break;
                } else if l == '[' || r == '[' || r == ',' || l == ',' {
                    // Do nothing
                } else {
                    // Numbers
                    let mut r_single_digit = true;
                    let mut l_single_digit = true;
                    if right.chars().nth(rc + 1).unwrap() != ',' && right.chars().nth(rc + 1).unwrap() != ']' {
                        r_single_digit = false;
                    }
                    if left.chars().nth(lc + 1).unwrap() != ',' && left.chars().nth(lc + 1).unwrap() != ']' {
                        l_single_digit = false;
                    }
                    // Wrong order
                    if (r_single_digit && l_single_digit && l as usize > r as usize) || (r_single_digit && !l_single_digit) {
                        wrong = true;
                        break;
                    } else {
                        // tc += 1;
                        break;
                    }
                }
            }
        }
        if wrong {
            return Ordering::Greater;
        } else {
            return Ordering::Less;
        }
    });
    // Print the let of items
    let i = packets.iter().position(|&p| p == "[[2]]").unwrap() + 1;
    let j = packets.iter().position(|&p| p == "[[6]]").unwrap() + 1;
    println!("Part 2: {}", i * j);
}
