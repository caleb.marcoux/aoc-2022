use std::fs;

fn main() {
    // Get input
    // let file_path = "20.example";
    let file_path = "20.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    const KEY: isize = 811589153;
    // const KEY: isize = 1;
    let numbers: Vec<isize> = input.split_terminator("\n").map(|x| x.parse::<isize>().unwrap() * KEY).collect();
    // Keep track of the old idices so we can find the number later.
    let mut mixed: Vec<(usize, isize)> = Vec::new();
    for (i, number) in numbers.iter().enumerate() {
        mixed.push((i, *number));
    }

    for _n in 0..10 {
        for (i, number) in numbers.iter().enumerate() {
            // Skip zeros
            if *number == 0 { continue; }
            // Find the current position of number
            let mix_i = mixed.iter().position(|m| m.0 == i).unwrap() as isize;
            // Remove from the old position
            mixed.remove(mix_i as usize);
            // Find the new position for the number
            let num_len = mixed.len() as isize;
            let offset = mix_i + number;
            let new_pos = offset.rem_euclid(num_len);
            // Insert into the new position
            mixed.insert(new_pos as usize, (i, *number));
        }
    }

    // Find 0 position
    let zero_i = mixed.iter().position(|m| m.1 == 0).unwrap();

    // 13148
    // -15909
    println!("{}", mixed[(zero_i + 1000) % mixed.len()].1);
    println!("{}", mixed[(zero_i + 2000) % mixed.len()].1);
    println!("{}", mixed[(zero_i + 3000) % mixed.len()].1);
    let t = mixed[(zero_i + 1000) % mixed.len()].1 + mixed[(zero_i + 2000) % mixed.len()].1 + mixed[(zero_i + 3000) % mixed.len()].1;
    println!("Part 1: {}", t);
}
