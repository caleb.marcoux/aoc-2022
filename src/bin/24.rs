use std::fs;
mod helpers;
use std::collections::HashSet;

fn print_blizzards(blizzards: &Vec<(helpers::Point, helpers::Point)>, width: i32, height: i32) {
    for _ in 1..width + 1 {
        print!("#");
    }
    println!();
    for j in 1..height - 1 {
        print!("#");
        for i in 1..width - 1 {
            let n = helpers::Point::new(i, j);
            if blizzards.contains(&(n, helpers::Point::new(1, 0))) {
                print!(">");
            } else if blizzards.contains(&(n, helpers::Point::new(0, 1))) {
                print!("v");
            } else if blizzards.contains(&(n, helpers::Point::new(-1, 0))) {
                print!("<");
            } else if blizzards.contains(&(n, helpers::Point::new(0, -1))) {
                print!("^");
            } else {
                print!(".");
            }
        }
        println!("#");
    }
    for _ in 1..width + 1 {
        print!("#");
    }
    println!();
}

fn main() {
    // Get input
    // let file_path = "24.example";
    let file_path = "24.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n").collect::<Vec<&str>>();

    const TRIPS: usize = 3;
    const DEBUG: bool = false;

    let mut blizzards: Vec<(helpers::Point, helpers::Point)> = Vec::new();
    let mut start = helpers::Point::new(1, 0);
    let mut end = helpers::Point::new(1, 0);
    let width = lines[0].len() as i32;
    let height = lines.len() as i32;

    // Fill in blizzards
    for (i, line) in lines.iter().enumerate() {
        for (j, c) in line.chars().enumerate() {
            if i == 0 {
                // On top edge
                if c == '.' {
                    start = helpers::Point::new(j as i32, i as i32);
                }
            } else if j == 0 || i == lines.len() - 1 || j == line.len() - 1 {
                // On other edge
                if c == '.' {
                    end = helpers::Point::new(j as i32, i as i32);
                }
            } else if  j < line.len() - 1 {
                // In the grid
                let mut dir = helpers::Point::new(0, 0);
                if c == '>' {
                    dir = helpers::Point::new(1, 0);
                } else if c == 'v' {
                    dir = helpers::Point::new(0, 1);
                } else if c == '<' {
                    dir = helpers::Point::new(-1, 0);
                } else if c == '^' {
                    dir = helpers::Point::new(0, -1);
                }
                // Add blizzard
                blizzards.push((helpers::Point::new(j as i32, i as i32), dir));
            }
        }
    }

    if DEBUG {
        println!("Start: {}", start);
        println!("End: {}", end);
        print_blizzards(&blizzards, width, height);
    }

    // Move each minute
    let mut min = 0;
    // Go back/forth
    for n in 0..TRIPS {
        // Path find to the end
        let mut choices: HashSet<helpers::Point> = HashSet::new();
        choices.insert(helpers::Point::new(start.x, start.y));
        'searching: loop {
            if min > 0 && min % 1000 == 0 {
                println!("min: {}", min);
            }
            // Simulate blizzards for this turn
            for (bliz, dir) in blizzards.iter_mut() {
                bliz.x = (bliz.x + dir.x - 1).rem_euclid(width - 2) + 1;
                bliz.y = (bliz.y + dir.y - 1).rem_euclid(height - 2) + 1;
            }
            // print_blizzards(&blizzards, width, height);
            let mut new_choices = HashSet::new();
            for choice in &choices {
                // Take valid choices for this turn
                let mut neightbors = choice.neighbors().to_vec();
                // Add stand still
                neightbors.push(*choice);
                let valid = neightbors.iter().filter_map(|n| {
                    // Space does not contain a blizzard
                    if !blizzards.contains(&(*n, helpers::Point::new(1, 0)))
                        && !blizzards.contains(&(*n, helpers::Point::new(0, 1)))
                        && !blizzards.contains(&(*n, helpers::Point::new(-1, 0)))
                        && !blizzards.contains(&(*n, helpers::Point::new(0, -1))) {
                        return Some(*n);
                    } else {
                        return None;
                    }
                }).collect::<Vec<helpers::Point>>();
                for v in valid {
                    // Check if we are at end
                    if v == end {
                        break 'searching;
                    }
                    if (v.x > 0 && v.x < width - 1 && v.y > 0 && v.y < height - 1)
                    || v == start || v == end {
                        new_choices.insert(v);
                    }
                }
            }
            // Update for next round
            choices = new_choices;
            min += 1;
            if choices.len() == 0 {
                println!("No choices!");
                break;
            }
        }
        min += 1;
        // Print part 1
        if n == 0 {
            println!("Part 1: {}", min);
        }
        // swap start/end
        let temp = start;
        start = end;
        end = temp;
        if DEBUG {
            println!("Start: {}", start);
            println!("End: {}", end);
            println!("len: {}", choices.len());
            for choice in choices {
                println!("c: {}", choice);
            }
        }
    }


    println!("Part 2: {}", min);
}
