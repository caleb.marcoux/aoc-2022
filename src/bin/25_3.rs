use std::fs;
mod helpers;
use std::collections::HashSet;
use rand::seq::SliceRandom;
use rand::Rng;
// use std::hash::Hash;

// Convert snafu to base 10
fn snafu(input: &Vec<char>) -> isize {
    let mut total: isize = 0;
    for (n, num) in input.iter().rev().enumerate() {
        let val = match num {
            '=' => -2,
            '-' => -1,
            '0' => 0,
            '1' => 1,
            '2' => 2,
            _ => 0,
        };
        total += val * 5_isize.pow(n as u32);
    }
    return total;
}

fn snafu_text(input: &[i8]) -> String {
    input.iter().map(|n| {
        match n {
            -2 => '=',
            -1 => '-',
            0 => '0',
            1 => '1',
            2 => '2',
            _ => 'N',
        }
    }).collect::<String>()
}

fn fit(value: &[i8], goal: isize) -> isize {
    // Find value of snafu chromosome.
    let mut total: isize = 0;
    let mut n = 1;
    for num in value.iter().rev() {
        total += *num as isize * n;
        n *= 5;
    }

    // Return difference between value and desired.
    (goal - total).abs()
}

fn mutate(value: &mut[i8]) {
    let mut rng = rand::thread_rng();
    let i = rng.gen_range(0..value.len());
    let e = value[i];
    // Change character
    let mut new = e;
    while new == e {
        new = *[-2, -1, 0, 1, 2].choose(&mut rng).unwrap();
    }
    // Update value array
    value[i] = new;
}

fn cross<const L: usize>(a: &[i8], b: &[i8]) -> ([i8; L], [i8; L]) {
    let mut rng = rand::thread_rng();
    let i = rng.gen_range(0..L);
    // Cross at this point
    let mut c = [0; L];
    let mut d = [0; L];
    for j in 0..L {
        if j < i {
            c[j] = b[j];
            d[j] = a[j];
        } else {
            c[j] = a[j];
            d[j] = b[j];
        }
    }
    return (c, d);
}

fn add_item<const L: usize>(next: &mut HashSet<[i8; L]>, item: [i8; L], best: &mut [i8; L], goal: isize, last_max: &mut isize, last_min: &mut isize, last_avg: isize) -> bool {
    let f = fit(&item, goal);
    // Check item
    if f == 0 {
        *best = item;
        return true;
    }
    if f > *last_max {
        *last_max = f;
    } else if f < *last_min {
        *last_min = f;
        *best = item;
    }
    // Check if this is good enough to insert
    if f < last_avg || next.len() < 100 {
        next.insert(item);
    }
    return false;
}

fn main() {
    // Get input
    // let file_path = "25.example";
    let file_path = "25.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut values = Vec::new();

    // Parse input
    for (_, line) in lines.enumerate() {
        values.push(snafu(&line.chars().collect::<Vec<char>>()));
    }

    helpers::print_vec(&values);

    // Add values
    let mut t = 0;
    for val in values {
        t += val;
    }

    // Find SNAFU number
    // by using a genetic algorithm!
    // Init population
    const LENGTH: usize = 21;
    // const LENGTH: usize = 8;
    const GEN_SIZE: usize = 50000;
    let mut population: HashSet<[i8; LENGTH]> = HashSet::new();
    population.insert([-2; LENGTH]);
    population.insert([-1; LENGTH]);
    population.insert([0; LENGTH]);
    population.insert([1; LENGTH]);
    population.insert([2; LENGTH]);
    // Run genetic algorithm
    let mut last_avg = 1000000000000000;
    let mut best = [0; LENGTH];
    'generations: loop {
        // Track last round's best/worst
        let mut last_min = 1000000000000000;
        let mut last_max = 0;
        // Create the next generation
        let mut next = HashSet::new();
        let mut last = [0; LENGTH];
        for (i, item) in population.iter().enumerate() {
            // Add back item unchanged
            if add_item(&mut next, *item, &mut best, t, &mut last_max, &mut last_min, last_avg) {
                break 'generations;
            }
            // Mutate
            let mut mutated = item.clone();
            mutate(&mut mutated);
            if add_item(&mut next, mutated, &mut best, t, &mut last_max, &mut last_min, last_avg) {
                break 'generations;
            }
            // Cross
            if i > 0 {
                let (a, b) = cross::<LENGTH>(item, &last);
                if add_item(&mut next, a, &mut best, t, &mut last_max, &mut last_min, last_avg)
                || add_item(&mut next, b, &mut best, t, &mut last_max, &mut last_min, last_avg) {
                    break 'generations;
                }
            }
            last = *item;
        }
        // Tournament selection to thin out population.
        while next.len() > GEN_SIZE {
            let next_vec: Vec<[i8; LENGTH]> = next.iter().cloned().collect();
            let mut next_iter = next_vec.iter();
            let mut a = next_iter.next().unwrap();
            for b in next_iter {
                if fit(a, t) < fit(b, t) {
                    next.remove(b);
                } else {
                    next.remove(a);
                }
                a = b;
            }
        }
        // Set next generation
        println!("len: {}", next.len());
        println!("min: {}", last_min);
        println!("max: {}", last_max);
        println!("best: {}", snafu_text(&best));
        population = next;
        last_avg = last_min + (last_max - last_min) / 4;
    }
    println!("best: {}", snafu_text(&best));

    println!();
    println!("Part 1: {}", t);
}
