use std::fs;
mod helpers;
use std::collections::HashSet;
// use std::collections::HashMap;
// use std::collections::VecDeque;

fn snafu(input: &Vec<char>) -> isize {
    let mut total: isize = 0;
    for (n, num) in input.iter().rev().enumerate() {
        let mut val: isize = 0;
        if *num == '2' {
            val = 2;
        } else if *num == '1' {
            val = 1;
        } else if *num == '0' {
            val = 0;
        } else if *num == '-' {
            val = -1;
        } else if *num == '=' {
            val = -2;
        }
        total += val * 5_isize.pow(n as u32);
    }
    return total;
}

fn main() {
    // Get input
    // let file_path = "25.example";
    let file_path = "25.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut values = Vec::new();

    // Parse input
    for (_, line) in lines.enumerate() {
        values.push(snafu(&line.chars().collect::<Vec<char>>()));
    }

    helpers::print_vec(&values);

    let mut t = 0;
    for val in values {
        t += val;
    }
    // TODO: REMOVE
    // t = 2022;
    // t = 12345;
    // t = 314159265;
    // t = 33841257499180 - 33841257484375;

    // Find SNAFU number
    let mut s = 0;
    let mut v: Vec<char> = Vec::new();
    let mut p = 0;
    // Find biggest place
    for n in 0.. {
        // Start bigger and subtract
        if s < t {
            s += 2 * 5_isize.pow(n as u32);
        } else {
            p = n;
            break;
        }
    }
    s = 0;
    let mut r; // Remainder

    // Manually try:
    // v.push('2');
    // p -= 1;
    // s = 38146972656250;
    // v.push('-');
    // p -= 1;
    // s = 34332275390625;

    loop {
        // Too big.
        let n_pow = if p > 0 {5_isize.pow((p - 1) as u32)} else {0};
        if 2 * n_pow >= t {
            if p == 0 {
                break;
            }
            p -= 1;
            continue;
        }
        r = t - s;
        // Greater than half
        let pow = 5_isize.pow(p as u32);
        println!("p: {}", pow);
        println!("n_p * 2: {}", n_pow * 2);
        println!("3p/2: {}", (pow + pow / 2));
        println!("r: {}", r);
        if r > (pow + pow / 2) {
            s += 2 * pow;
            v.push('2');
        } else if r > 0 {
            s += pow;
            v.push('1');
        } else if r.abs() <= pow / 2 {
            v.push('0');
        } else if -r > (pow + pow / 2) {
            s -= 2 * pow;
            v.push('=');
        } else {
            s -= pow;
            v.push('-');
        }
        println!("s: {}", s);
        helpers::print_vec(&v);
        if p == 0 {
            break;
        }
        p -= 1;
    }

    helpers::print_vec(&v);

    // // Just search for the right answer.
    // let mut choices: HashSet<(isize, Vec<char>)> = HashSet::new();
    // choices.insert((-2, Vec::from(['='])));
    // choices.insert((-1, Vec::from(['-'])));
    // choices.insert((0, Vec::from(['0'])));
    // choices.insert((1, Vec::from(['1'])));
    // choices.insert((2, Vec::from(['2'])));
    // let mut found: Vec<char> = Vec::new();
    // 'finding: for n in 1.. {
    //     println!("n: {}", n);
    //     let mut new_choices = HashSet::new();
    //     for choice in choices {
    //         for (val, c) in [(-2, '='), (-1, '-'), (0, '0'), (1, '1'), (2, '2')] {
    //             let val = choice.0 + (val * 5_isize.pow(n as u32));
    //             // helpers::print_vec(&new_vec.iter().rev().collect());
    //             if val % 5 != t % 5 {
    //                 continue;
    //             }
    //             let mut new_vec = choice.1.clone();
    //             if val == t {
    //                 found = new_vec;
    //                 break 'finding;
    //             }
    //             new_vec.push(c);
    //             new_choices.insert((val, new_vec));
    //         }
    //     }
    //     choices = new_choices;
    // }
    // println!("{}", s);
    //
    // helpers::print_vec(&found.iter().rev().collect());
    // Check answer!
    println!("Back again: {}", snafu(&v));
    for val in v {
        print!("{}", val);
    }

    println!();
                                             // 10-=210
    println!("Manual: {}", snafu(&"2--2-0=--0--100-=210".chars().collect::<Vec<char>>()));
    println!("Correct: 2--2-0=--0--100-=210");
    // let mut t = 0;
    println!("Part 1: {}", t);
    // println!("Part 2: {}", t);
}
