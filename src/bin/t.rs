fn main() {
    let a: &str = "hello";
    let b: &str = "you";
    let v = vec![a, b];
    let r: &str = &*format!("{}/{}", a, b);
    // let r: &str = v.join("-").to_string();
    println!("{}", r);
}
