use std::fs;
mod helpers;
use rand::seq::SliceRandom;

// Convert snafu to base 10
fn snafu(input: &Vec<char>) -> isize {
    let mut total: isize = 0;
    for (n, num) in input.iter().rev().enumerate() {
        let val = match num {
            '=' => -2,
            '-' => -1,
            '0' => 0,
            '1' => 1,
            '2' => 2,
            _ => 0,
        };
        total += val * 5_isize.pow(n as u32);
    }
    return total;
}

fn snafu_text(input: &[i8]) -> String {
    input.iter().map(|n| {
        match n {
            -2 => '=',
            -1 => '-',
            0 => '0',
            1 => '1',
            2 => '2',
            _ => 'N',
        }
    }).collect::<String>()
}

fn fit(value: &[i8], goal: isize) -> isize {
    // Find value of snafu chromosome.
    let mut total: isize = 0;
    let mut n = 1;
    for num in value.iter().rev() {
        total += *num as isize * n;
        n *= 5;
    }

    // Return difference between value and desired.
    (goal - total).abs()
}

// Mutate at the approximate position that needs to change
fn smart_mutate(value: &mut[i8], goal: isize) {
    // Find place
    let f = fit(value, goal);
    let mut p = 1;
    let mut i = value.len();
    while p <= f {
        p *= 5;
        i -= 1;
    }
    let e = value[i];
    // Change character
    let mut new = e;
    let mut rng = rand::thread_rng();
    while new == e {
        new = *[-2, -1, 0, 1, 2].choose(&mut rng).unwrap();
    }
    // Update value array
    value[i] = new;
}

fn main() {
    // Get input
    // let file_path = "25.example";
    let file_path = "25.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut values = Vec::new();

    // Parse input
    for (_, line) in lines.enumerate() {
        values.push(snafu(&line.chars().collect::<Vec<char>>()));
    }

    helpers::print_vec(&values);

    // Add values
    let mut t = 0;
    for val in values {
        t += val;
    }

    // Find SNAFU number
    // by smartly mutating an array of 0s.
    const LENGTH: usize = 25;
    // const LENGTH: usize = 8;
    // Run "smart genetic" algorithm
    let mut best = [0; LENGTH];
    loop {
        // Smart mutate
        smart_mutate(&mut best, t);
        let f = fit(&best, t);
        if f == 0 {
            break;
        }
        // Print best current number
        println!("num: {}", snafu_text(&best));
    }
    println!();

    println!("num: {}", snafu_text(&best));
    println!("Part 1: {}", t);
}
