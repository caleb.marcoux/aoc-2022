use std::fs;

fn snafu(input: &Vec<char>) -> isize {
    let mut total: isize = 0;
    for (n, num) in input.iter().rev().enumerate() {
        let val = match num {
            '=' => -2,
            '-' => -1,
            '0' => 0,
            '1' => 1,
            '2' => 2,
            _ => 0,
        };
        total += val * 5_isize.pow(n as u32);
    }
    return total;
}

fn main() {
    // Get input
    // let file_path = "25.example";
    let file_path = "25.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    // let lines = input.split_terminator("\n\n");
    let lines = input.split_terminator("\n");

    let mut values = Vec::new();

    // Parse input
    for (_, line) in lines.enumerate() {
        values.push(snafu(&line.chars().collect::<Vec<char>>()));
    }

    // Add values
    let mut t = 0;
    for val in values {
        t += val;
    }

    // Find SNAFU number
    let mut v: Vec<char> = Vec::new();
    let mut r = t; // Remainder
    loop {
        v.push(['=', '-', '0', '1', '2'][((r + 2) % 5) as usize]);
        r = (r + 2) / 5;
        if r == 0 {
            break;
        }
    }
    v.reverse();

    // Check answer!
    println!("Back again: {}", snafu(&v));
    // Print answer
    for val in v {
        print!("{}", val);
    }

    println!();
    println!("Part 1: {}", t);
}
