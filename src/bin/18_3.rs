use std::fs;
use std::collections::HashSet;

type Cube = (i32, i32, i32);

fn surrounding (cube: Cube) -> Vec<Cube> {
    let mut s = Vec::new();
    for d in [(0, 0, 1), (0, 0, -1), (0, 1, 0), (0, -1, 0), (1, 0, 0), (-1, 0, 0)] {
        s.push((cube.0 + d.0, cube.1 + d.1, cube.2 + d.2));
    }
    return s;
}

fn main() {
    // Get input
    // let file_path = "18.example";
    let file_path = "18.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let lines: Vec<&str> = input.split_terminator('\n').collect();

    let mut cubes: HashSet<Cube> = HashSet::new();
    let mut spaces: HashSet<Cube> = HashSet::new();

    for line in lines {
        let coords: Vec<i32> = line.split_terminator(',')
            .map(|c| c.parse().unwrap()).collect();
        let x = coords[0];
        let y = coords[1];
        let z = coords[2];
        cubes.insert((x, y, z));
    }

    let mut t = 0;
    // Check how many faces are free.
    for cube in &cubes {
        for s in surrounding(*cube) {
            if !cubes.contains(&s) {
                spaces.insert(s);
                t += 1;
            }
        }
    }

    // Start searching from all spaces
    let mut added: HashSet<Cube> = HashSet::new();
    'checking: for space in &spaces {
        if added.contains(space) {
            continue 'checking;
        }
        let mut open: HashSet<Cube> = HashSet::new();
        open.insert(*space);
        let mut explored: HashSet<Cube> = HashSet::new();
        let mut last_explored: isize = -1;
        while open.len() > 0 && explored.len() as isize != last_explored {
            last_explored = explored.len() as isize;
            let mut new_open = HashSet::new();
            for o in &open {
                if !cubes.contains(o) && !explored.contains(o) {
                    // Add posibilities
                    for s in surrounding(*o) {
                        new_open.insert(s);
                    }
                    explored.insert(*o);
                }
                if o.0 < 0 || o.1 < 0 || o.2 < 0 {
                    // We must be in the open
                    continue 'checking;
                }
            }
            open = new_open;
        }
        added.insert(*space);
    }

    // Subtrack added cubes.
    let mut p_2 = t;
    for cube in &added {
        for s in surrounding(*cube) {
            if cubes.contains(&s) {
                p_2 -= 1;
            }
        }
    }

    // println!("ADDED: {}", added.len());
    println!("Part 1: {}", t);
    println!("Part 2: {}", p_2);
}
