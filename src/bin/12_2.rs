use std::fs;
use std::collections::HashSet;

fn main() {
    // Get input
    // let file_path = "12.example";
    let file_path = "12.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let rows = input.split_terminator("\n");

    let mut grid: Vec<Vec<usize>> = Vec::new();
    // let mut start = (0, 0);
    let mut end = (0, 0);

    // Create map
    let mut i: usize = 0;
    let mut j: usize;
    // Gather starting points.
    let mut starts = Vec::new();
    for row in rows {
        j = 0;
        grid.push(row.chars().map(|x| {
            j += 1;
            let p = (i, j - 1);
            if x == 'S' {
                // Set start point
                starts.push(p);
                return 0
            } else if x == 'E' {
                // Set end point
                end = p;
                return 25
            } else if x == 'a' {
                // Add as potential start.
                starts.push(p);
                return 0
            } else {
                return x as usize - 'a' as usize
            }
        }).collect::<Vec<usize>>());
        i += 1;
    }

    // Track best start lenght
    let mut best = 600;
    // Start search for each point
    for start in starts {
        // println!("Start: ({}, {})", start.0, start.1);
        let mut points = vec![(1, start)];
        let mut explored = HashSet::new();
        explored.insert(start);
        let mut length: usize = 0;
        let mut found = false;
        while !found && points.len() > 0 {
            let mut new_points = Vec::new();
            for point in points {
                let p = point.1;
                let path = point.0;
                // Get available moves.
                let mut moves = Vec::new();
                if p.0 > 0 {
                    moves.push((p.0 - 1, p.1));
                }
                if p.1 > 0 {
                    moves.push((p.0, p.1 - 1));
                }
                if p.0 < grid.len() - 1 {
                    moves.push((p.0 + 1, p.1));
                }
                if p.1 < grid[0].len() - 1 {
                    moves.push((p.0, p.1 + 1));
                }
                let valid = moves.iter().filter(|m| {
                    return grid[m.0][m.1] <= grid[p.0][p.1] + 1 && !explored.contains(m);
                }).collect::<Vec<_>>();
                // Spawn children for each.
                for v in valid {
                    if *v == end {
                        length = path;
                        found = true;
                        break;
                    }
                    new_points.push((path + 1, *v));
                    explored.insert(*v);
                }
            }
            points = new_points;
        }
        if length <= best && found {
            best = length;
        }
    }
    println!("success!");
    println!("Length of path: {}", best);
}
