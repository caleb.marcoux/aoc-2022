use std::fs;

fn find_marker<const LENGTH: usize>(input: &String) {
    let mut recent = [input.chars().nth(0).unwrap(); LENGTH];
    for (i, c) in input.chars().enumerate() {
        recent.rotate_left(1);
        let mut different = true;
        for (j, r) in recent.iter().enumerate() {
            for (k, n) in recent.iter().enumerate() {
                if n == r && k != j {
                    different = false;
                }
            }
        }
        if different && i >= LENGTH {
            println!("{}", i);
            break;
        }
        recent[LENGTH - 1] = c;
    }
}

fn main() {
    // Get input
    let file_path = "6.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");

    // Part 1
    find_marker::<4>(&input);
    // Part 2
    find_marker::<14>(&input);
}

