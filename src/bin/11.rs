use std::fs;

fn main() {
    // Get input
    let file_path = "11.example";
    // let file_path = "11.input";
    let input = fs::read_to_string(file_path)
        .expect("Could not read input");
    let starting = input.split_terminator("\n\n").collect::<Vec<&str>>();
    // let commands = input.split_terminator("\n\n");
    // let commands = input.split_terminator("\n").collect::<Vec<&str>>();
    
    let mut monkeys: Vec<Vec<u64>> = Vec::new();
    let mut inspected: Vec<u64> = vec![0; 8];

    // Build starting monkeys.
    for start in starting.iter() {
        for (i, line) in start.split("\n").enumerate() {
            let sides = line.split(": ").collect::<Vec<&str>>();
            if i == 1 {
                monkeys.push(sides[1].split(", ").map(|x| x.parse().unwrap()).collect::<Vec<u64>>());
            }
        }
    }

    // for monkey in &monkeys {
    //     for item in monkey {
    //         print!("{}, ", *item);
    //     }
    //     println!("");
    // }

    for n in 0..20 {
        // println!("{}", n);
        for (j, start) in starting.iter().enumerate() {
            // Count items inspected
            inspected[j] += monkeys[j].len() as u64;
            // Monkey loop
            let mut divis = 0;
            let mut true_recip = 0;
            let mut false_recip = 0;
            // Monkey steps
            for (i, line) in start.split("\n").enumerate() {
                let sides = line.split(": ").collect::<Vec<&str>>();
                for monkey in &monkeys {
                    for item in monkey {
                        print!("{}, ", *item);
                    }
                    println!("");
                }
                // WORRY LEVELS.
                if i == 2 {
                    let ops = sides[1].split(" ").collect::<Vec<&str>>();
                    for item in monkeys[j].iter_mut() {
                        let mut a: u64 = *item;
                        let mut b: u64 = *item;
                        let mut ans: u64 = 0;
                        if ops[4] != "old" {
                            b = ops[4].parse().unwrap();
                        }
                        if ops[3] == "+" {
                            ans = a + b;
                        } else if ops[3] == "-" {
                            ans = a - b;
                        } else if ops[3] == "*" {
                            ans = a * b;
                        }
                        // Assign new worry levels.
                        // AND DIVIDE!
                        *item = ans;
                    }
                // TEST!
                } else if i == 3 {
                    divis = sides[1].split("divisible by ").collect::<Vec<&str>>()[1].parse().unwrap();
                    // println!("Divis: {}", divis);
                } else if i == 4 {
                    true_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                } else if i == 5 {
                    false_recip = sides[1].split("throw to monkey ").collect::<Vec<&str>>()[1].parse().unwrap();
                    // TEST AND THROW ITEMS:
                    let mut items = monkeys[j].clone();
                    for item in items.iter() {
                        if *item % divis == 0 {
                            monkeys[true_recip].push(*item);
                        } else {
                            monkeys[false_recip].push(*item);
                        }
                    }
                    // Remove items from self
                    monkeys[j] = Vec::new();
                }
            }
        }
    }

    // for insp in &inspected {
    //     println!("{}", insp);
    // }
    inspected.sort();
    println!("Part 1: {}", inspected[inspected.len()-1] * inspected[inspected.len()-2]);
}
