use std::ops::{Add, Sub, Range};
use std::fmt::{Display, Formatter, Result};
use std::cmp;

#[derive(Hash, PartialEq, Eq, Clone, Copy)]
pub struct Point3 {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Point3 {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        Point3 { x, y, z }
    }

    pub fn neighbors(self) -> [Self; 6] {
        [
            Point3::new(self.x + 1, self.y, self.z),
            Point3::new(self.x - 1, self.y, self.z),
            Point3::new(self.x, self.y + 1, self.z),
            Point3::new(self.x, self.y - 1, self.z),
            Point3::new(self.x, self.y, self.z + 1),
            Point3::new(self.x, self.y, self.z - 1),
        ]
    }
}

impl Add for Point3 {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Point3 {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Display for Point3 {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}


#[derive(Hash, PartialEq, Eq, Clone, Copy)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x, y }
    }

    pub fn neighbors(self) -> [Point; 4] {
        [
            Point::new(self.x + 1, self.y),
            Point::new(self.x - 1, self.y),
            Point::new(self.x, self.y + 1),
            Point::new(self.x, self.y - 1),
        ]
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

pub fn smart_range(s: isize, e: isize) -> Range<isize> {
    return Range { start: cmp::min(s, e), end: cmp::max(s, e) };
}
// pub fn smart_range(s: isize, e: isize) -> Iter<'static, isize> {
//     if s < e {
//         return (Range { start: cmp::min(s, e), end: cmp::max(s, e) + 1 }).iter();
//     } else {
//         return (Range { start: cmp::min(s, e), end: cmp::max(s, e) + 1 }).into_iter();
//     }
// }

pub fn get_ints(input: &str) -> Vec<isize> {
    input.split_terminator([' ', ':', ',', '=', ';', '.']).filter_map(|t| t.parse::<isize>().ok()).collect::<Vec<isize>>()
}

pub fn print_vec<T: Display>(input: &Vec<T>) {
    for n in input {
        print!("{}, ", n);
    }
    println!();
}

pub fn print_array<T: Display>(input: &[T]) {
    for n in input {
        print!("{}, ", n);
    }
    println!();
}
